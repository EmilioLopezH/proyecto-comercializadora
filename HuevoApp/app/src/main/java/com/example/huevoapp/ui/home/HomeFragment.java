package com.example.huevoapp.ui.home;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.huevoapp.MainActivity;
import com.example.huevoapp.R;
import com.example.huevoapp.cargaImagen;
import com.example.huevoapp.pedidoActivity;
import com.example.huevoapp.repartidorActivity;
import com.example.huevoapp.repartidorMainActivity;
import com.example.huevoapp.sesionActivity;
import com.example.huevoapp.productoActivity;
import com.google.firebase.FirebaseError;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class HomeFragment extends Fragment { //PRODUCTOS

    private DatabaseReference dbRef;
    private DatabaseReference dbRefR, dbRefR2,dbRefR3,dbRefR4,dbRefR5;
    private int count=0;
    ArrayList<producto> listaProducto;
    RecyclerView recyclerProducto;
    public ProgressDialog miprogreso;
    private HomeViewModel homeViewModel;
    private Button btnInsertar;
    private String idUsuario=null;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        // final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });

        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            // Si hay conexión a Internet en este momento
        } else {
            new AlertDialog.Builder(getContext())
                    .setTitle("Error").setMessage("No es posible la conexión").setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().finish();
                }
            }).show();        }

        idUsuario = (String) getActivity().getIntent().getSerializableExtra("idUsuario");

        btnInsertar = root.findViewById(R.id.btnInsertar);
        btnInsertar.setVisibility(View.GONE);
        btnInsertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*dbRef = FirebaseDatabase.getInstance().getReference();
                Toast.makeText(getActivity() , "Se ha pulsado el btnInsertar: ", Toast.LENGTH_LONG).show();
                Map<String, Object> datos = new HashMap<>();
                datos.put("tipo","Carton de Leche Lala bajo en grasa");
                datos.put("descripcion","MIL HOSTIAS");
                datos.put("precio","25.23");
                Toast.makeText(getActivity() , "datosInsert: "+datos, Toast.LENGTH_LONG).show();
               dbRef.child("producto").push().setValue(datos);*/
                //Intent i = new Intent(getContext(), cargaImagen.class);
                  //  startActivity(i);
              /* dbRefR = FirebaseDatabase.getInstance().getReference("usuario/-M8QuuH5JFvjRTt-uxaw/pedidoy/");
                dbRefR.goOnline();
                String llave = dbRefR.push().getKey();
                Map<String, Object> datos = new HashMap<>();
                datos.put("fecha","las 19 am");
                datos.put("direccion","direcciosdsada");
                datos.put("lat",-10);
                datos.put("lon",-19);
                datos.put("precios","recibo");
                datos.put("productos","compraLista");
                datos.put("total",25.00);
                datos.put("folio",llave);
                datos.put("estado","falso");
                datos.put("idUsuario","ejemplo");
                datos.put("idRepartidor","ejemplo");
                dbRefR.goOffline();
                dbRefR.child(llave).setValue(datos);*/

              ///// FUNCIONALIDAD ACEPTAR PEDIDO EN REPARTIDOR

               /* dbRefR = FirebaseDatabase.getInstance().getReference("usuario/-M8QuuH5JFvjRTt-uxaw/pedido/").child("-MB1fApTFglYtYE6eHCr");
                dbRefR.goOnline();

                Map<String, Object> datos = new HashMap<>();
                datos.put("estado","activo");
                datos.put("idRepartidor","-MB1WLQcwSx45j_MMKo6");

                dbRefR.updateChildren(datos);
                dbRefR.goOffline();

                dbRefR2 = FirebaseDatabase.getInstance().getReference("repartidor/-MB1WLQcwSx45j_MMKo6/pedidox/-MB1fApTFglYtYE6eHCr/");
                dbRefR3 = FirebaseDatabase.getInstance().getReference("repartidor/pedido/-MB1fApTFglYtYE6eHCr/");
                mueveChild(dbRefR3,dbRefR2);*/

               /* Intent i = new Intent(v.getContext(), repartidorMainActivity.class);
                i.putExtra("idRepartidor","-MB1WLQcwSx45j_MMKo6");
                v.getContext().startActivity(i);
*/
               Intent intent = new Intent(getContext(), MainActivity.class);
                showNotification(getContext(), "Preba", "Has entregado un pedido satisfactoriamente", intent, 1);



            }
        });
        listaProducto = new ArrayList<>();
        recyclerProducto = root.findViewById(R.id.recyclerProducto);
        recyclerProducto.setLayoutManager(new LinearLayoutManager(getContext()));
        miprogreso = new ProgressDialog(getActivity());
/*       listaUsuario = new ArrayList<>();n
        recyclerUsuario = root.findViewById(R.id.recyclerUsuario);
        recyclerUsuario.setLayoutManager(new LinearLayoutManager(getContext()));

        llenarlistaUsuario();
        adaptador adaptadorUsuario = new adaptador(listaUsuario);
        recyclerUsuario.setAdapter(adaptadorUsuario);*/


      /*  btnInsertar = root.findViewById(R.id.btnInsertar);
        btnInsertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity() , "Se ha pulsado el btnInsertar: ", Toast.LENGTH_LONG).show();
                Map<String, Object> datos = new HashMap<>();
                datos.put("nombre","Elmo");
                datos.put("direccion","Elmo");
                datos.put("correo","Elmo");
                datos.put("telefono","Elmo");
                Toast.makeText(getActivity() , "datosInsert: "+datos, Toast.LENGTH_LONG).show();
                dbRef.child("usuario").push().setValue(datos);
            }
        });*/



        cargarProductos();

        return root;
    }

    private void cargarProductos(){
        miprogreso.setMessage("Espera...");
        miprogreso.show();
        dbRef = FirebaseDatabase.getInstance().getReference("producto/");
        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
    dbRef.goOnline();
    dbRef.addValueEventListener(new ValueEventListener() {
        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            for (final DataSnapshot snapshot : dataSnapshot.getChildren()){
                dbRef.child(snapshot.getKey()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        producto datosProducto = snapshot.getValue(producto.class);
                        listaProducto.add(new producto(datosProducto.getTipo(), datosProducto.getDescripcion(), datosProducto.getPrecio(),snapshot.getKey(),idUsuario));
                        adaptador adaptadorProducto = new adaptador(listaProducto);
                        recyclerProducto.setAdapter(adaptadorProducto);
                        miprogreso.dismiss();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
            }
        }
        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    });
    dbRef.goOffline();

}
    public void showNotification(Context context, String title, String message, Intent intent, int reqCode) {
        //SharedPreferenceManager sharedPreferenceManager = SharedPreferenceManager.getInstance(context);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, reqCode, intent, PendingIntent.FLAG_ONE_SHOT);
        String CHANNEL_ID = "channel_name";// The id of the channel.
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.mipmap.loma_foreground)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Channel Name";// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);
        }
        notificationManager.notify(reqCode, notificationBuilder.build()); // 0 is the request code, it should be unique id

        Log.d("showNotification", "showNotification: " + reqCode);
    }


}
