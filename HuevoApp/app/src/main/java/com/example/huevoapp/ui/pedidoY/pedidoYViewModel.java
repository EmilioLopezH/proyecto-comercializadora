package com.example.huevoapp.ui.pedidoY;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class pedidoYViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public pedidoYViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is a fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
