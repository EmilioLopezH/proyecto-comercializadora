package com.example.huevoapp.ui.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.huevoapp.MainActivity;
import com.example.huevoapp.MapsActivity;
import com.example.huevoapp.R;
import com.example.huevoapp.productoActivity;
import com.example.huevoapp.sesionActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.NoSuchElementException;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class adaptador extends RecyclerView.Adapter<adaptador.lista> {  //Producto RecyclerView

    ArrayList<producto> listaProducto;
    DecimalFormat formatter = new DecimalFormat("#,###.00");

    public adaptador(ArrayList<producto> listaProducto) {
        this.listaProducto = listaProducto;
    }

    @NonNull
    @Override
    public lista onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_home,parent,false);
        return new lista(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final lista holder, final int position) {
        final String tipo = listaProducto.get(position).getTipo();
        final String precio=listaProducto.get(position).getPrecio();
        final String idProducto=listaProducto.get(position).getIdProducto();
        final String idUsuario=listaProducto.get(position).getIdUsuario();

        holder.textoTipo.setText(tipo);
        holder.textoPrecio.setText("$ "+formatter.format(Double.parseDouble(precio))+" MXN");

        final StorageReference mImageRef =
                FirebaseStorage.getInstance().getReference("/imagen/producto/"+idProducto+".jpg");
        System.out.println("PATH:          "+mImageRef.getPath());

        final long ONE_MEGABYTE = 1024 * 1024;
        mImageRef.getBytes(ONE_MEGABYTE)
                .addOnSuccessListener(new OnSuccessListener<byte[]>() {
                    @Override
                    public void onSuccess(byte[] bytes) {
                        Bitmap bm = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        holder.img.setImageBitmap(bm);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
            }
        });

        holder.LinearProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), productoActivity.class);
                    i.putExtra("idProducto", idProducto);
                    i.putExtra("idUsuario", idUsuario);
                    v.getContext().startActivity(i);

            }});
    }

    @Override
    public int getItemCount() {
        return listaProducto.size();
    }

    public class lista extends RecyclerView.ViewHolder {
    TextView textoTipo, textoPrecio;
    ImageView img;
    LinearLayout LinearProducto;
        public lista(@NonNull View itemView) {
            super(itemView);
            textoTipo = (TextView) itemView.findViewById(R.id.textView4);
            textoPrecio = (TextView) itemView.findViewById(R.id.textView11);
            LinearProducto = (LinearLayout) itemView.findViewById(R.id.LinearHome);
            img = (ImageView) itemView.findViewById(R.id.imageProducto);
        }
    }

}
