package com.example.huevoapp.ui.dashboard;

public class carrito {
    private String tipo;
    private String descripcion;
    private String precio;
    private String cantidad;
    private String precioTotal;
    private String idUsuario;
    private String idProducto;
    private String idCarrito;


    public carrito(){

    }

    public carrito(String tipo, String descripcion, String precio, String cantidad, String precioTotal, String idUsuario, String idProducto, String idCarrito) {
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.precio = precio;
        this.cantidad = cantidad;
        this.precioTotal = precioTotal;
        this.idUsuario = idUsuario;
        this.idProducto = idProducto;
        this.idCarrito = idCarrito;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(String precioTotal) {
        this.precioTotal = precioTotal;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdProducto() { return idProducto; }

    public void setIdProducto(String idProducto) { this.idProducto = idProducto; }

    public String getIdCarrito() {
        return idCarrito;
    }

    public void setIdCarrito(String idCarrito) {
        this.idCarrito = idCarrito;
    }
}
