package com.example.huevoapp.ui.pedidoY;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.huevoapp.R;
import com.example.huevoapp.verpedidoActivity;

import java.text.DecimalFormat;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class adaptadorPedidoY extends RecyclerView.Adapter<adaptadorPedidoY.lista> {
    DecimalFormat formatter = new DecimalFormat("#,###.00");
    ArrayList<pedidoY> listaPedido;

    public adaptadorPedidoY(ArrayList<pedidoY> listaPedido) {
        this.listaPedido = listaPedido;
    }

    @NonNull
    @Override
    public adaptadorPedidoY.lista onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_pedidoy,parent,false);
        return new lista(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final adaptadorPedidoY.lista holder, final int position) {
        final String folio = listaPedido.get(position).getFolio();
        final String fecha=listaPedido.get(position).getFecha();
        final String direcc = listaPedido.get(position).getDireccion();
        final Double precio=listaPedido.get(position).getTotal();


        holder.textFolio.setText(folio);
        holder.textFecha.setText(fecha);
        holder.textDireccion.setText(direcc);
        holder.textPrecio.setText("$ "+formatter.format(Double.parseDouble(precio.toString()))+" MXN");

        holder.linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext() , "informacion para idUsuario: "+listaPedido.get(position).getIdUsuario(), Toast.LENGTH_LONG).show();
                Toast.makeText(v.getContext() , "informacion para IdRep: "+listaPedido.get(position).getIdRepartidor(), Toast.LENGTH_LONG).show();
                Toast.makeText(v.getContext() , "informacion para idPedido: "+folio, Toast.LENGTH_LONG).show();
                Intent i = new Intent(v.getContext(), verpedidoActivity.class);
                if(listaPedido.get(position).getIdUsuario()!=null)
                    i.putExtra("idUsuario",listaPedido.get(position).getIdUsuario());
                else if(listaPedido.get(position).getIdRepartidor()!=null)
                    i.putExtra("idRepartidor",listaPedido.get(position).getIdRepartidor());
                i.putExtra("idPedido",folio);
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaPedido.size();
    }

    public class lista extends RecyclerView.ViewHolder {
        TextView textFolio, textFecha, textPrecio, textDireccion;
        LinearLayout linear;

        public lista(@NonNull View itemView) {
            super(itemView);
            textFolio = (TextView) itemView.findViewById(R.id.textFolioY);
            textFecha = (TextView) itemView.findViewById(R.id.textFechaY);
            textPrecio = (TextView) itemView.findViewById(R.id.textTotalPedidoY);
            textDireccion = (TextView) itemView.findViewById(R.id.textDireccY);
            linear = itemView.findViewById(R.id.linearY);
        }
    }
}
