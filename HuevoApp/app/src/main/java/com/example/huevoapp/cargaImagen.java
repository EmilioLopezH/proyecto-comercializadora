package com.example.huevoapp;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

public class cargaImagen extends AppCompatActivity {

    Button cargaImagen, subirImagen;
    ImageView imagen;
    ProgressDialog miprogreso;
    boolean imagenCargada=false;
    File imagenFile;
    private static final int GALLERY_REQUEST = 123;
    String idProducto="-M91uC25CWZ8dZEH-TwE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_carga_imagen);

        cargaImagen = findViewById(R.id.btnSubir);
        subirImagen = findViewById(R.id.btnUpload);
        imagen = findViewById(R.id.imageView3);
        miprogreso = new ProgressDialog(cargaImagen.this);

        cargaImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                miprogreso.setMessage("Espera...");
                miprogreso.show();
                Intent i = new Intent(Intent.ACTION_PICK);
                i.setType("image/*");
                startActivityForResult(i, GALLERY_REQUEST);

            }
        });

        subirImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                miprogreso.setMessage("Espera...");
                miprogreso.show();
                agregaImagen(idProducto);

            }
        });

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    CONTEXT_INCLUDE_CODE);
            return;
        }else{
            System.out.println("Accediendo...");
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK)
            switch (requestCode){
                case GALLERY_REQUEST:
                    Uri selectedImage = data.getData();
                    File imageFile = new File(getRealPathFromURI(selectedImage));
                    System.out.println("fila imagen nombre: "+imageFile.getName());
                    System.out.println("fila imagen path: "+imageFile.getPath());
                    System.out.println("fila imagen: "+imageFile);
                    System.out.println("imagen: "+selectedImage);
                    try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImage);
                        int height = bitmap.getHeight(), width = bitmap.getWidth();
                        if(height > 4096 || width > 4096){
                            System.out.println("ES GIGANTE");
                           Bitmap rawTakenImage = BitmapFactory.decodeFile(imageFile.getPath());
                           Bitmap resizedBitmap = BitmapScaler.scaleToFitWidth(rawTakenImage, 720);
                            imagen.setImageBitmap(resizedBitmap);
                            imagenCargada = true;
                            OutputStream outStream = new FileOutputStream(imageFile);
                            resizedBitmap.compress(Bitmap.CompressFormat.JPEG,100,outStream);
                            outStream.flush();
                            outStream.close();
                            imagenFile=imageFile;
                        }
                        else{
                        imagen.setImageBitmap(bitmap);
                            imagenCargada = true;
                            imagenFile=imageFile;
                        }
                    } catch (IOException e) {
                        System.out.println("Some exception " + e);
                    }
                    break;
            }
        miprogreso.dismiss();
    }

    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) {
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private void agregaImagen(String idProducto){
        if(imagenCargada==true){
            StorageReference miStorage = FirebaseStorage.getInstance().getReference();
            StorageReference dbRef = miStorage.child("/imagen/producto/"+idProducto+".jpg");

            dbRef.putFile(Uri.fromFile(new File(imagenFile.getPath()))).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    System.out.println(Uri.fromFile(new File(imagenFile.getPath())));
                    Toast.makeText(getApplicationContext(), "La imagen se ha subido exitosamente a la nube", Toast.LENGTH_LONG).show();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    System.out.println(Uri.fromFile(new File(imagenFile.getPath())));
                    Toast.makeText(getApplicationContext(), "Error al subir imagen a la nube", Toast.LENGTH_LONG).show();
                }
            });

            miprogreso.dismiss();
            finish();

        }else{
            Toast.makeText(getApplicationContext(), "No se ha cargado ninguna imagen", Toast.LENGTH_LONG).show();
            miprogreso.dismiss();
        }
    }

}




