package com.example.huevoapp.ui.pedidoX;


import android.content.Context;

public class pedidoX {
    private String fecha;
    private String direccion;
    private Double lat;
    private Double lon;
    private String listaPrecio;
    private String listaProduto;
    private Double total;
    private String folio;
    private String estado;
    private String idUsuario;
    private String idRepartidor;
    private Context con;

    public pedidoX(){

    }

    public pedidoX(String fecha, String direccion, Double lat, Double lon, String listaPrecio, String listaProduto, Double total, String folio, String estado, String idUsuario, String idRepartidor, Context con) {
        this.fecha = fecha;
        this.direccion = direccion;
        this.lat = lat;
        this.lon = lon;
        this.listaProduto = listaProduto;
        this.listaPrecio = listaPrecio;
        this.total = total;
        this.folio = folio;
        this.estado = estado;
        this.idUsuario=idUsuario;
        this.idRepartidor=idRepartidor;
        this.con=con;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getListaProduto() {
        return listaProduto;
    }

    public void setListaProduto(String listaProduto) {
        this.listaProduto = listaProduto;
    }

    public String getListaPrecio() {
        return listaPrecio;
    }

    public void setListaPrecio(String listaPrecio) {
        this.listaPrecio = listaPrecio;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getIdRepartidor() {
        return idRepartidor;
    }

    public void setIdRepartidor(String idRepartidor) {
        this.idRepartidor = idRepartidor;
    }

    public Context getCon() {
        return con;
    }

    public void setCon(Context con) {
        this.con = con;
    }
}