package com.example.huevoapp.ui.notifications;

public class usuario {

    private String nombre;
    private String password;
    private String telefono;
    private String direccion;

    public usuario() {
    }
    public usuario(String nombre, String password, String telefono, String direccion) {
        this.nombre=nombre;
        this.password=password;
        this.telefono=telefono;
        this.direccion=direccion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

}
