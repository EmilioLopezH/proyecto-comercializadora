package com.example.huevoapp.ui.pedidoZ;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.huevoapp.R;
import com.example.huevoapp.entregaActivity;
import com.example.huevoapp.verpedidoActivity;

import java.text.DecimalFormat;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class adaptadorPedidoZ extends RecyclerView.Adapter<adaptadorPedidoZ.lista> {
    DecimalFormat formatter = new DecimalFormat("#,###.00");
    ArrayList<pedidoZ> listaPedido;

    public adaptadorPedidoZ(ArrayList<pedidoZ> listaPedido) {
        this.listaPedido = listaPedido;
    }

    @NonNull
    @Override
    public adaptadorPedidoZ.lista onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_pedidoz,parent,false);
        return new lista(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final adaptadorPedidoZ.lista holder, final int position) {
        final String folio = listaPedido.get(position).getFolio();
        final String fecha=listaPedido.get(position).getFecha();
        final String direcc = listaPedido.get(position).getDireccion();
        final Double precio=listaPedido.get(position).getTotal();


        holder.textFolio.setText(folio);
        holder.textFecha.setText(fecha);
        holder.textDireccion.setText(direcc);
        holder.textPrecio.setText("$ "+formatter.format(Double.parseDouble(precio.toString()))+" MXN");

        holder.btnEstado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), entregaActivity.class);
                i.putExtra("idPedido",listaPedido.get(position).getFolio());
                i.putExtra("idRepartidor",listaPedido.get(position).getIdRepartidor());
                i.putExtra("idUsuario",listaPedido.get(position).getIdUsuario());
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaPedido.size();
    }

    public class lista extends RecyclerView.ViewHolder {
        TextView textFolio, textFecha, textPrecio, textDireccion;
        Button btnEstado;
        public lista(@NonNull View itemView) {
            super(itemView);
            textFolio = (TextView) itemView.findViewById(R.id.textFolioZ);
            textFecha = (TextView) itemView.findViewById(R.id.textFechaZ);
            textPrecio = (TextView) itemView.findViewById(R.id.textTotalPedidoZ);
            textDireccion = (TextView) itemView.findViewById(R.id.textDireccZ);
            btnEstado = itemView.findViewById(R.id.btnSeguimientoZ);

        }
    }
}
