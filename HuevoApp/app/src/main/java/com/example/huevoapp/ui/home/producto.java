package com.example.huevoapp.ui.home;

public class producto {

    private String tipo;
    private String precio;
    private String descripcion;
    private String idProducto;
    private String idUsuario;

    public producto() {
    }
    public producto(String tipo, String descripcion , String precio, String idProducto, String idUsuario) {
        this.tipo=tipo;
        this.descripcion=descripcion;
        this.precio=precio;
        this.idProducto=idProducto;
        this.idUsuario=idUsuario;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) { this.precio = precio; }

    public String getDescripcion() { return descripcion; }

    public void setDescripcion(String descripcion) { this.descripcion = descripcion; }

    public String getIdProducto() { return idProducto; }

    public void setIdProducto(String idProducto) { this.idProducto = idProducto; }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(String idUsuario) {
        this.idUsuario = idUsuario;
    }
}
