package com.example.huevoapp;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.huevoapp.ui.notifications.usuario;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient location;
    private Location geoLocation;
    private TextView textAddress;
    private ResultReceiver resultReceiver;
    private Button btnConfirmar;
    Marker miMarcador;
    String miDireccion="";
    String idUsuario=null;
    String compraLista="";
    String recibo="";
    Double precios=0.0;
    String nuevaDireccion="";
    Double lat, lon;
    ProgressDialog miprogreso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("welcome");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        textAddress = findViewById(R.id.textAddress);
        btnConfirmar = findViewById(R.id.btnConfirmarD);
        miprogreso = new ProgressDialog(this);
        resultReceiver = new AddressResultReceiver(new Handler());
       //idUsuario = (String) getIntent().getSerializableExtra("idUsuario");
        compraLista = (String) getIntent().getSerializableExtra("compraLista");
        precios = (Double) getIntent().getSerializableExtra("precios");
        recibo = (String) getIntent().getSerializableExtra("recibo");
        idUsuario = (String) getIntent().getSerializableExtra("idUsuario");

        Toast.makeText(this, "compraLista: "+compraLista, Toast.LENGTH_LONG).show();
        Toast.makeText(this, "precios: "+precios, Toast.LENGTH_LONG).show();
        Toast.makeText(this, "recibo: "+recibo, Toast.LENGTH_LONG).show();


        btnConfirmar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(MapsActivity.this);
                View mV = getLayoutInflater().inflate(R.layout.dialog_direccion,null);
                final EditText editDireccion = mV.findViewById(R.id.editDireccion);
                Button aceptar = mV.findViewById(R.id.btnAceptarD);
                Button cancelar = mV.findViewById(R.id.btnCancelarD);
                editDireccion.setText(miDireccion);

                aceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nuevaDireccion = editDireccion.getText().toString();
                        guardarDireccion(idUsuario);
                        finish();
                    }
                });

                cancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finish();
                    }
                });
                mBuilder.setView(mV);
                AlertDialog dialog = mBuilder.create();
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);

        // Add a marker in Sydney and move the camera
        LatLng morelia = new LatLng(19.7027116, -101.1923818);
        mMap.addMarker(new MarkerOptions().position(morelia).title("Estas en Morelia, Mich."));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(morelia, 10f));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(this, "No cuenta con los permisos", Toast.LENGTH_LONG).show();
            return;
        }

        tuUbicacion();

        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {

                actualizarUbicacion(geoLocation);
                System.out.println("Actuaizar...............");
                handler.postDelayed(this, 25000);
            }
        };

        handler.postDelayed(r, 25000);

    }


    private void fetchAddressFromLatLon(Location l) {
        Intent intent = new Intent(this, buscaDireccion.class);
        intent.putExtra(Constants.RECEIVER, resultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, l);
        startService(intent);

    }

    private class AddressResultReceiver extends ResultReceiver {
        AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            if (resultCode == Constants.SUCCESS_RESULT) {
                miDireccion = resultData.getString(Constants.RESULT_DATA_KEY);
                textAddress.setText(resultData.getString(Constants.RESULT_DATA_KEY));
            } else {
                System.out.println("Valio madres " + resultData.getString(Constants.RESULT_DATA_KEY));
            }
        }
    }

    private void tuUbicacion() {

        location = LocationServices.getFusedLocationProviderClient(this);
        System.out.println("Entras a tu Ubicacion");
        System.out.println("Location location"+ location);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        location.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                System.out.println("Entras a last location succes lsitener");
                if (location != null) {
                    geoLocation = location;
                    lat=location.getLatitude();
                    lon=location.getLongitude();
                    LatLng mi = new LatLng(lat, lon);
                    miMarcador = mMap.addMarker(new MarkerOptions().position(mi).title("Tu estás aqui").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                    // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,10f));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mi,16f));

                    Location lo = new Location("providerNA");
                    lo.setLatitude(lat);
                    lo.setLongitude(lon);
                    fetchAddressFromLatLon(lo);
                } else {
                    System.out.println("error");
                }
            }
        });
    }

    private void actualizarUbicacion(Location loc){
        double lat, lon;
        lat=loc.getLatitude();
        lon=loc.getLongitude();
        LatLng mi = new LatLng(lat, lon);
        miMarcador.remove();
        miMarcador = mMap.addMarker(new MarkerOptions().position(mi).title("Tu estás aqui").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,10f));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mi,16f));

        Location lo = new Location("providerNA");
        lo.setLatitude(lat);
        lo.setLongitude(lon);
        fetchAddressFromLatLon(lo);
    }

    private void guardarDireccion(String idU){
        if (compraLista!=null || precios!=null){
            //guarda pedidoY
           guardarPedido(idU,compraLista,precios);
            Toast.makeText(this, "lat: "+lat+" lon:"+lon, Toast.LENGTH_LONG).show();
            Toast.makeText(this, "Se ha realizado un nuevo pedido a "+nuevaDireccion, Toast.LENGTH_LONG).show();
        }else{
            //edita direccion de usuario
            Toast.makeText(this, "Se ha actualizado tu direccion a "+nuevaDireccion, Toast.LENGTH_LONG).show();
            DatabaseReference dbRefDireccion = FirebaseDatabase.getInstance().getReference("usuario/").child(idU);
            dbRefDireccion.goOnline();

            Map<String, Object> datos = new HashMap<>();
            datos.put("direccion",nuevaDireccion);
            dbRefDireccion.updateChildren(datos);
            dbRefDireccion.goOffline();
        }
    }

    private void guardarPedido(String idUsuario, final String compraLista, final Double precios){
        //miprogreso.setMessage("Espera...");
        //miprogreso.show();
        final String idU = idUsuario;
        final Date currentTime = Calendar.getInstance().getTime();
        System.out.println(""+currentTime.toString());
        DatabaseReference dbRefUsuario = FirebaseDatabase.getInstance().getReference("usuario/"+idU+"/pedido");
        final String llave = dbRefUsuario.push().getKey();

        dbRefUsuario.goOnline();
        Map<String, Object> datos = new HashMap<>();
        datos.put("fecha",currentTime.toString());
        datos.put("direccion",nuevaDireccion);
        datos.put("lat",lat);
        datos.put("lon",lon);
        datos.put("precios",recibo);
        datos.put("productos",compraLista);
        datos.put("total",precios+25.00);
        datos.put("folio",llave);
        datos.put("estado","falso");
        datos.put("notas","ingresa una nota");
        dbRefUsuario.child(llave).setValue(datos).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                DatabaseReference dbRefCarrito = FirebaseDatabase.getInstance().getReference("usuario/"+idU);
                dbRefCarrito.goOnline();
                dbRefCarrito.child("carrito").removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //manda pedido a repartidor (pedidos pendientes)
                        DatabaseReference dbRefRep = FirebaseDatabase.getInstance().getReference("repartidor/pedido");
                        dbRefRep.goOnline();
                        Map<String, Object> datos = new HashMap<>();
                        datos.put("fecha",currentTime.toString());
                        datos.put("direccion",nuevaDireccion);
                        datos.put("lat",lat);
                        datos.put("lon",lon);
                        datos.put("precios",recibo);
                        datos.put("productos",compraLista);
                        datos.put("total",precios+25.00);
                        datos.put("folio",llave);
                        datos.put("estado","falso");
                        datos.put("notas","ingresa una nota");
                        datos.put("idUsuario",idU);
                        datos.put("idRepartidor",null);
                        dbRefRep.child(llave).setValue(datos).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Intent i = new Intent(getApplicationContext(),pedidoActivity.class);
                                i.putExtra("idUsuario",idU);
                                i.putExtra("idPedido",llave);
                                startActivity(i);
                                finish();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                                i.putExtra("idUsuario",idU);
                                startActivity(i);
                                finish();
                            }
                        });
                        dbRefRep.goOffline();
                        //miprogreso.dismiss();


                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //miprogreso.dismiss();

                    }
                });
                dbRefCarrito.goOffline();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
        dbRefUsuario.goOffline();


    }

}
