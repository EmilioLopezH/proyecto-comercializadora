package com.example.huevoapp.ui.pedidoX;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class pedidoXViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public pedidoXViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is a fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
