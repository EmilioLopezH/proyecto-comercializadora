package com.example.huevoapp.ui.pedidoW;


public class pedidoW {
    private String fecha;
    private String direccion;
    private Double lat;
    private Double lon;
    private String precios;
    private String productos;
    private Double total;
    private String folio;
    private String estado;
    private String idUsuario;
    private String idRepartidor;

    public pedidoW(){

    }

    public pedidoW(String fecha, String direccion, Double lat, Double lon, String precios, String productos, Double total, String folio, String estado, String idUsuario, String idRepartidor) {
        this.fecha = fecha;
        this.direccion = direccion;
        this.lat = lat;
        this.lon = lon;
        this.precios = precios;
        this.productos = productos;
        this.total = total;
        this.folio = folio;
        this.estado = estado;
        this.idUsuario=idUsuario;
        this.idRepartidor=idRepartidor;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getIdUsuario() { return idUsuario; }

    public void setIdUsuario(String idUsuario) { this.idUsuario = idUsuario; }

    public String getPrecios() {
        return precios;
    }

    public void setPrecios(String precios) {
        this.precios = precios;
    }

    public String getProductos() {
        return productos;
    }

    public void setProductos(String productos) {
        this.productos = productos;
    }

    public String getIdRepartidor() {
        return idRepartidor;
    }

    public void setIdRepartidor(String idRepartidor) {
        this.idRepartidor = idRepartidor;
    }
}