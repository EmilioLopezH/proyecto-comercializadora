package com.example.huevoapp.ui.dashboard;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.huevoapp.MainActivity;
import com.example.huevoapp.MapsActivity;
import com.example.huevoapp.R;
import com.example.huevoapp.compraActivity;
import com.example.huevoapp.sesionActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


public class DashboardFragment extends Fragment { //Carrito

    private DashboardViewModel dashboardViewModel;
    public ProgressDialog miprogreso;
    private int count=0;
    ArrayList<carrito> listaCarrito;
    RecyclerView recyclerCarrito;
    private TextView NumCarrito;
    private Button comprar;

    String idUsuario=null;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
       // final TextView textView = root.findViewById(R.id.text_dashboard);
        dashboardViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
        //        textView.setText(s);
            }
        });

        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            // Si hay conexión a Internet en este momento
        } else {
            new AlertDialog.Builder(getContext())
                    .setTitle("Error").setMessage("No es posible la conexión").setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().finish();
                }
            }).show();        }
        miprogreso = new ProgressDialog(getActivity());
        listaCarrito = new ArrayList<>();
        recyclerCarrito = root.findViewById(R.id.recyclerCarrito);
        recyclerCarrito.setLayoutManager(new LinearLayoutManager(getContext()));
        NumCarrito = root.findViewById(R.id.textCarrito);
        comprar = root.findViewById(R.id.btnComprar);
        idUsuario = (String) getActivity().getIntent().getSerializableExtra("idUsuario");

        cargarCarrito(idUsuario);

        comprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getContext(), compraActivity.class);
                i.putExtra("idUsuario",idUsuario);
                startActivity(i);
            }
        });

      /*  btnInsertar = root.findViewById(R.id.btnInsertar);
        btnInsertar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity() , "Se ha pulsado el btnInsertar: ", Toast.LENGTH_LONG).show();
                Map<String, Object> datos = new HashMap<>();
                datos.put("nombre","Elmo");
                datos.put("direccion","Elmo");
                datos.put("correo","Elmo");
                datos.put("telefono","Elmo");
                Toast.makeText(getActivity() , "datosInsert: "+datos, Toast.LENGTH_LONG).show();
                dbRef.child("usuario").push().setValue(datos);
            }
        });*/

        /*dbRef.child("usuario").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Log.i(null,"sasadasdasd 2");

                for (final DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Toast.makeText(getActivity() , "snapshot FOR: "+snapshot, Toast.LENGTH_LONG).show();
                    count++;

                    dbRef.child("usuario").child(snapshot.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            usuario datosUsuario = snapshot.getValue(usuario.class);
                            Toast.makeText(getActivity() , "datosUsuario: "+datosUsuario.getNombre()+" "+datosUsuario.getCorreo()+" "+datosUsuario.getDireccion()+" "+datosUsuario.getTelefono(), Toast.LENGTH_LONG).show();
                            listaUsuario.add(new usuario(datosUsuario.getNombre(), datosUsuario.getCorreo(), datosUsuario.getDireccion(), datosUsuario.getTelefono()));
                            adaptador adaptadorUsuario = new adaptador(listaUsuario);
                            recyclerUsuario.setAdapter(adaptadorUsuario);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                    String datosSnap = snapshot.getValue().toString();
                    Toast.makeText(getActivity() , "datosSnap: ( "+count+" ) "+datosSnap, Toast.LENGTH_LONG).show();

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });*/
       // Intent intent = new Intent(view.getContext(),RegistrarUsr.class);
       // startActivity(intent);
        DatabaseReference dbRefConsulta = FirebaseDatabase.getInstance().getReference("usuario/"+idUsuario+"/carrito/producto/");
        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
        dbRefConsulta.goOnline();
        dbRefConsulta.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                listaCarrito.clear();
                recyclerCarrito.setAdapter(null);
                recyclerCarrito.setLayoutManager(new LinearLayoutManager(getActivity()));
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefConsulta.goOffline();
        return root;
    }

    void cargarCarrito(final String idU) {
        miprogreso.setMessage("Espera...");
        miprogreso.show();
        final DatabaseReference dbRefCarrito = FirebaseDatabase.getInstance().getReference("usuario/"+idU+"/carrito/producto/"); //+idU+"/carrito/producto
        System.out.println("MI ruta al carrito: "+dbRefCarrito);
        dbRefCarrito.goOnline();
        dbRefCarrito.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount()==1){
                    NumCarrito.setText("Carrito: "+dataSnapshot.getChildrenCount()+" item"); }
                else if(dataSnapshot.getChildrenCount()<1 && idU.equals("nadie")){
                    NumCarrito.setText("Registrate");
                    comprar.setVisibility(View.GONE);
                    miprogreso.dismiss();
                    android.app.AlertDialog.Builder mBuilder = new android.app.AlertDialog.Builder(getContext());
                    LayoutInflater li = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View mV = li.inflate(R.layout.dialog_direccion,null);
                    EditText editDireccion = mV.findViewById(R.id.editDireccion);
                    editDireccion.setVisibility(View.GONE) ;
                    TextView texto = mV.findViewById(R.id.textView12);
                    texto.setText("Registrate para agregar a carrito");
                    Button aceptar = mV.findViewById(R.id.btnAceptarD);
                    aceptar.setText("Registrar");
                    Button cancelar = mV.findViewById(R.id.btnCancelarD);
                    cancelar.setText("NO");
                    mBuilder.setView(mV);
                    final android.app.AlertDialog dialog = mBuilder.create();
                    aceptar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPreferences preferences = getActivity().getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.clear().apply();
                            Intent i = new Intent(v.getContext(), sesionActivity.class);
                            startActivity(i);
                            dialog.dismiss();
                            getActivity().finish();
                        }
                    });
                    cancelar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    // dialog.setCancelable(false);
                    // dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }else if(dataSnapshot.getChildrenCount()<1){
                    NumCarrito.setText("Agrega productos");
                    comprar.setVisibility(View.GONE);
                    miprogreso.dismiss();
                }
                else{
                NumCarrito.setText("Carrito: "+dataSnapshot.getChildrenCount()+" items");}
                for (final DataSnapshot snapshot : dataSnapshot.getChildren()){
                    dbRefCarrito.child(snapshot.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            System.out.println("dentro "+snapshot.getChildrenCount());
                            carrito datosCarrito = snapshot.getValue(carrito.class);
                            listaCarrito.add(new carrito(datosCarrito.getTipo(),datosCarrito.getDescripcion(), datosCarrito.getPrecio(), datosCarrito.getCantidad(), datosCarrito.getPrecioTotal(),datosCarrito.getIdUsuario(),datosCarrito.getIdProducto(),datosCarrito.getIdCarrito()));
                            adaptador adaptadorCarrito = new adaptador(listaCarrito);
                            recyclerCarrito.setAdapter(adaptadorCarrito);
                            miprogreso.dismiss();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefCarrito.goOffline();
    }

}


