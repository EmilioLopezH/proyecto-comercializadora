package com.example.huevoapp.ui.pedidoW;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.huevoapp.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class pedidoWFragment extends Fragment {
    private pedidoWViewModel pedidoYViewModel;
    String idRepartidor=null;
    ProgressDialog miprogreso;
    ArrayList<pedidoW> listaPedido;
    RecyclerView recyclerPedido;
    Boolean b = null;
    Button regresar;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        pedidoYViewModel = ViewModelProviders.of(this).get(pedidoWViewModel.class);
        View root = inflater.inflate(R.layout.fragment_pedidox, container, false);
        // final TextView textView = root.findViewById(R.id.text_home);
        pedidoYViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });
        miprogreso = new ProgressDialog(getActivity());
       // idUsuario = (String) getActivity().getIntent().getSerializableExtra("idUsuario");
        idRepartidor = (String) getActivity().getIntent().getSerializableExtra("idRepartidor");
        listaPedido = new ArrayList<>();
        recyclerPedido = root.findViewById(R.id.recyclerPedidoX);
        recyclerPedido.setLayoutManager(new LinearLayoutManager(getContext()));
        regresar = root.findViewById(R.id.btnRegresar);
        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        //regresar = root.findViewById(R.id.btnRegresarX);
       /* regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });*/
        DatabaseReference dbRefConsulta = FirebaseDatabase.getInstance().getReference("repartidor/pedido/");
        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
        dbRefConsulta.goOnline();
        dbRefConsulta.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                listaPedido.clear();
                recyclerPedido.setAdapter(null);
                recyclerPedido.setLayoutManager(new LinearLayoutManager(getActivity()));
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                listaPedido.clear();
                recyclerPedido.setAdapter(null);
                recyclerPedido.setLayoutManager(new LinearLayoutManager(getActivity()));

                //cargarPedidos(idUsuario);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                listaPedido.clear();
                recyclerPedido.setAdapter(null);
                recyclerPedido.setLayoutManager(new LinearLayoutManager(getActivity()));
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefConsulta.goOffline();


        cargarPedidos();
        return root;
    }
    private void cargarPedidos(){
        // miprogreso.setMessage("Espera...");
        // miprogreso.show();
        final DatabaseReference dbRefPedido = FirebaseDatabase.getInstance().getReference("repartidor/pedido/");
        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
        dbRefPedido.goOnline();
        dbRefPedido.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot : dataSnapshot.getChildren()){
                    dbRefPedido.child(snapshot.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            pedidoW datosPedido = snapshot.getValue(pedidoW.class);
                            listaPedido.add(new pedidoW(datosPedido.getFecha(), datosPedido.getDireccion(),datosPedido.getLat(),datosPedido.getLon(),datosPedido.getPrecios(),datosPedido.getProductos(),datosPedido.getTotal(),datosPedido.getFolio(),datosPedido.getEstado(),datosPedido.getIdUsuario(),idRepartidor));
                            adaptadorPedidoW adaptadorW = new adaptadorPedidoW(listaPedido);
                            recyclerPedido.setAdapter(adaptadorW);
                            //   miprogreso.dismiss();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        dbRefPedido.goOffline();

    }
}
