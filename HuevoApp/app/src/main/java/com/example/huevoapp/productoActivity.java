package com.example.huevoapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.text.InputFilter;
import android.widget.Toast;

import com.example.huevoapp.ui.home.producto;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.sql.SQLOutput;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class productoActivity extends AppCompatActivity {

    DecimalFormat formatter = new DecimalFormat("#,###.00");
private TextView precioTotal, precioProducto, tipoProducto, descProducto;
private EditText cantidadProducto;
private Button btnCancelar, btnAgregar;
private ImageView imagen;
private String tipo,desc,precio,cantidad="1",precioTot,idProducto, idUsuario;
    ProgressDialog miprogreso;
    AlertDialog.Builder mBuilder;
    View mV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_producto);

        // tipo = (String) getIntent().getSerializableExtra("tipo");
      //  desc = (String) getIntent().getSerializableExtra("desc");
       // precio = (String) getIntent().getSerializableExtra("precio");
        idProducto = (String) getIntent().getSerializableExtra("idProducto");
        idUsuario = (String) getIntent().getSerializableExtra("idUsuario");
        precioProducto = findViewById(R.id.precioProducto);
        tipoProducto = findViewById(R.id.tipoProducto);
        descProducto = findViewById(R.id.descProducto);
        precioTotal = findViewById(R.id.totalProducto);
        cantidadProducto = findViewById(R.id.editText5);
        cantidadProducto.setFilters( new InputFilter[]{ new MinMaxFilter( "1" , "1000" )}) ;
        btnCancelar = findViewById(R.id.btnCancelar);
        btnAgregar = findViewById(R.id.btnAgregar);
        imagen = findViewById(R.id.imageProductoE);
        miprogreso = new ProgressDialog(this);
        mBuilder = new AlertDialog.Builder(productoActivity.this);
        mV = getLayoutInflater().inflate(R.layout.dialog_direccion,null);
        consultarProducto(idProducto);

        cantidadProducto.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

try {
    int factorCantidad = Integer.parseInt(s.toString());
    double factorUnitario = Double.parseDouble(precio);
    precioTot=""+factorCantidad*factorUnitario;
    cantidad=""+factorCantidad;
}catch (Exception e){
    precioTot=precio;
    cantidad="1";
}
precioTotal.setText("Precio Total: $ "+formatter.format(Double.parseDouble(precioTot))+" MXN");

            }
        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
if(idUsuario.equals("nadie")){
    btnAgregar.setText("REGISTRATE AQUI PARA COMPRAR");
    btnAgregar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            SharedPreferences preferences = v.getContext().getSharedPreferences("credenciales", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.clear().apply();
            Intent i = new Intent(v.getContext(), sesionActivity.class);
            startActivity(i);
            finish();
        }
    });
}else{
    btnAgregar.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            agregarCarrito(idUsuario);
        }
    });

}
      //  InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
      //  imm.hideSoftInputFromWindow(cantidad.getWindowToken(),0);

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

    }

    private void consultarProducto(final String idP) {
        miprogreso.setMessage("Espera...");
        miprogreso.show();

        final DatabaseReference dbRefProducto;
        dbRefProducto = FirebaseDatabase.getInstance().getReference("producto/"+idP);
        dbRefProducto.goOnline();
        dbRefProducto.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    try {
                        //JSONObject json = new JSONObject(dataSnapshot.getValue().toString());
                        producto datosProducto = dataSnapshot.getValue(producto.class);
                        tipo=datosProducto.getTipo();
                        desc=datosProducto.getDescripcion();
                        precio=datosProducto.getPrecio();
                        precioTot=precio;
                        final StorageReference mImageRef =
                                FirebaseStorage.getInstance().getReference("/imagen/producto/"+idP+".jpg");
                        System.out.println("PATH:          "+mImageRef.getPath());

                        final long ONE_MEGABYTE = 1024 * 1024;
                        mImageRef.getBytes(ONE_MEGABYTE)
                                .addOnSuccessListener(new OnSuccessListener<byte[]>() {
                                    @Override
                                    public void onSuccess(byte[] bytes) {
                                        Bitmap bm = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                        imagen.setImageBitmap(bm);
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                            }
                        });
                        dbRefProducto.goOffline();

                        tipoProducto.setText(tipo);
                        descProducto.setText(desc);
                        precioProducto.setText("Precio Unitario $ "+formatter.format(Double.parseDouble(precio))+" MXN");
                        precioTotal.setText("Precio Total $ "+formatter.format(Double.parseDouble(precioTot))+" MXN");

                    } catch (Exception e) {
                        System.out.println("ERROR!!! "+e);
                    }
                    miprogreso.dismiss();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

        });
    }

    private void agregarCarrito(String idUsuario){
        miprogreso.setMessage("Espera...");
        miprogreso.show();
        String idU = idUsuario;
        DatabaseReference dbRefUsuario;
        dbRefUsuario = FirebaseDatabase.getInstance().getReference("usuario/"+idU+"/carrito/producto");
        String llave = dbRefUsuario.push().getKey();
                        dbRefUsuario.goOnline();
                        Map<String, Object> datos = new HashMap<>();
                        datos.put("tipo",tipo);
                        datos.put("descripcion",desc);
                        datos.put("precioUnitario",precio);
                        datos.put("cantidad",cantidad);
                        datos.put("precioTotal",precioTot);
                        datos.put("idUsuario",idU);
                        datos.put("idProducto",idProducto);
                        datos.put("idCarrito",llave);
                        dbRefUsuario.child(llave).setValue(datos).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {

                                EditText editDireccion = mV.findViewById(R.id.editDireccion);
                                editDireccion.setVisibility(View.GONE) ;
                                TextView texto = mV.findViewById(R.id.textView12);
                                texto.setText("Se ha agregado exitosamente al carrito");
                                Button aceptar = mV.findViewById(R.id.btnAceptarD);
                                aceptar.setText("OK");
                                Button cancelar = mV.findViewById(R.id.btnCancelarD);
                                cancelar.setVisibility(View.GONE) ;
                                mBuilder.setView(mV);
                                final AlertDialog dialog = mBuilder.create();
                                dialog.setCancelable(false);
                                dialog.setCanceledOnTouchOutside(false);
                                dialog.show();
                                aceptar.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();

                                        finish();
                                    }
                                });

                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {

                                EditText editDireccion = mV.findViewById(R.id.editDireccion);
                                TextView texto = mV.findViewById(R.id.textView12);
                                editDireccion.setVisibility(View.GONE) ;
                                texto.setText("No se ha podido agregar a carrito");
                               Button aceptar = mV.findViewById(R.id.btnAceptarD);
                                aceptar.setVisibility(View.GONE) ;
                                Button cancelar = mV.findViewById(R.id.btnCancelarD);
                                cancelar.setText("OK");
                                mBuilder.setView(mV);
                                final AlertDialog dialog = mBuilder.create();
                                dialog.setCancelable(false);
                                dialog.setCanceledOnTouchOutside(false);
                                cancelar.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        finish();
                                    }
                                });

                                dialog.show();
                            }
                        });
                        dbRefUsuario.goOffline();
                        miprogreso.dismiss();
                        /*if (json.getString("contraseña").equals(pass)) {
                            System.out.println("login correcto !!!");
                            ban=true;
                        }*/


        //Toast.makeText(getActivity() , "datosInsert: "+datos, Toast.LENGTH_LONG).show();
    }
}
