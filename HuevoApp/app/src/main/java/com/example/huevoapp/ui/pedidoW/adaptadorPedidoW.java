package com.example.huevoapp.ui.pedidoW;

import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.huevoapp.R;
import com.example.huevoapp.repartidorActivity;

import java.text.DecimalFormat;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class adaptadorPedidoW extends RecyclerView.Adapter<adaptadorPedidoW.lista> {
    DecimalFormat formatter = new DecimalFormat("#,###.00");
    ArrayList<pedidoW> listaPedido;

    public adaptadorPedidoW(ArrayList<pedidoW> listaPedido) {
        this.listaPedido = listaPedido;
    }

    @NonNull
    @Override
    public adaptadorPedidoW.lista onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_pedidox,parent,false);
        return new lista(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final adaptadorPedidoW.lista holder, final int position) {
        final String folio = listaPedido.get(position).getFolio();
        final String fecha=listaPedido.get(position).getFecha();
        final String direcc = listaPedido.get(position).getDireccion();
        final Double precio=listaPedido.get(position).getTotal();


        holder.textFolio.setText(folio);
        holder.textFecha.setText(fecha);
        holder.textDireccion.setText(direcc);
        holder.textPrecio.setText("$ "+formatter.format(Double.parseDouble(precio.toString()))+" MXN");

        holder.btnEstado.setTextColor(Color.BLUE);
        holder.btnEstado.setText("VER NUEVO PEDIDO");
        holder.btnEstado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), repartidorActivity.class);
                i.putExtra("idPedido",folio);
             i.putExtra("idRepartidor",listaPedido.get(position).getIdRepartidor());
                v.getContext().startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaPedido.size();
    }

    public class lista extends RecyclerView.ViewHolder {
        TextView textFolio, textFecha, textPrecio, textDireccion;
        Button btnEstado;

        public lista(@NonNull View itemView) {
            super(itemView);
            textFolio = (TextView) itemView.findViewById(R.id.textFolio);
            textFecha = (TextView) itemView.findViewById(R.id.textFecha);
            textPrecio = (TextView) itemView.findViewById(R.id.textTotalPedido);
            textDireccion = (TextView) itemView.findViewById(R.id.textDireccP);
            btnEstado = itemView.findViewById(R.id.btnSeguimiento);
        }
    }
}
