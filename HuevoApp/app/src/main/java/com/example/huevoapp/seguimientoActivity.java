package com.example.huevoapp;
import com.example.huevoapp.ui.pedidoX.pedidoX;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.huevoapp.ui.notifications.usuario;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class seguimientoActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient location;
    private Location geoLocation;
    private ResultReceiver resultReceiver;
    Marker miMarcador;
    Marker repartidor=null;
    String miDireccion="";
    String idRepartidor="";
    String idUsuario=null;
    String idPedido="";
    String compraLista="";
    String recibo="";
    Double precios=0.0;
    TextView textAddress;
    Double lat, lon;
    TextView notas, notasRep, textNom;
    Button verNota;
    ProgressDialog miprogreso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("welcome");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seguimiento);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapS);
        mapFragment.getMapAsync(this);

        resultReceiver = new AddressResultReceiver(new Handler());
        textAddress = findViewById(R.id.textAddressS);
        idPedido = (String) getIntent().getSerializableExtra("idPedido");
        notasRep = findViewById(R.id.textNota3);
        notas = findViewById(R.id.textNotaYo2);
        verNota = findViewById(R.id.btnVerPedido3);
        textNom= findViewById(R.id.textNota2);
        idUsuario = (String) getIntent().getSerializableExtra("idUsuario");


        buscaRepartidor(idUsuario);


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);

        // Add a marker in Sydney and move the camera
       // LatLng morelia = new LatLng(19.7027116, -101.1923818);
       // mMap.addMarker(new MarkerOptions().position(morelia).title("Estas en Morelia, Mich."));
       // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(morelia, 10f));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(this, "No cuenta con los permisos", Toast.LENGTH_LONG).show();
            return;
        }

        tuUbicacion();

        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {

                //actualizarUbicacion(geoLocation);
                System.out.println("Actuaizar...............");
                handler.postDelayed(this, 30000);
            }
        };

        handler.postDelayed(r, 30000);

    }


    private void fetchAddressFromLatLon(Location l) {
        Intent intent = new Intent(this, buscaDireccion.class);
        intent.putExtra(Constants.RECEIVER, resultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, l);
        startService(intent);

    }

    private class AddressResultReceiver extends ResultReceiver {
        AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            if (resultCode == Constants.SUCCESS_RESULT) {
                miDireccion = resultData.getString(Constants.RESULT_DATA_KEY);
                textAddress.setText(resultData.getString(Constants.RESULT_DATA_KEY));
            } else {
                System.out.println("Valio madres " + resultData.getString(Constants.RESULT_DATA_KEY));
            }
        }
    }

    private void tuUbicacion() {
        location = LocationServices.getFusedLocationProviderClient(this);
        System.out.println("Location location"+ location);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        location.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    geoLocation = location;
                    lat=location.getLatitude();
                    lon=location.getLongitude();
                    LatLng mi = new LatLng(lat, lon);
                    miMarcador = mMap.addMarker(new MarkerOptions().position(mi).title("Tu estás aqui").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                    //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,10f));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mi,12f));

                    Location lo = new Location("providerNA");
                    lo.setLatitude(lat);
                    lo.setLongitude(lon);
                    fetchAddressFromLatLon(lo);
                } else {
                    System.out.println("error");
                }
            }
        });
    }

    private void actualizarUbicacion(Location loc){
        double lat, lon;
        lat=loc.getLatitude();
        lon=loc.getLongitude();
        LatLng mi = new LatLng(lat, lon);
        miMarcador.remove();
        miMarcador = mMap.addMarker(new MarkerOptions().position(mi).title("Tu estás aqui").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,10f));
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mi,16f));

        //actualiza Lat Lon en pedido
        DatabaseReference dbRefActualiza = FirebaseDatabase.getInstance().getReference("repartidor/pedido/").child(idPedido);
        dbRefActualiza.goOnline();
        Map<String, Object> datos = new HashMap<>();
        datos.put("lat",lat);
        datos.put("lon",lon);
        dbRefActualiza.updateChildren(datos);
        dbRefActualiza.goOffline();

        Location lo = new Location("providerNA");
        lo.setLatitude(lat);
        lo.setLongitude(lon);
        fetchAddressFromLatLon(lo);
    }


    private void buscaRepartidor(final String idU){
        DatabaseReference dbRefUsuario = FirebaseDatabase.getInstance().getReference("usuario/"+idU+"/pedido/"+idPedido+"/");
        dbRefUsuario.goOnline();
        dbRefUsuario.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                pedidoX datosPed = dataSnapshot.getValue(pedidoX.class);

                idRepartidor = datosPed.getIdRepartidor();

                notasRep.setText(dataSnapshot.child("notas").getValue().toString());
                DatabaseReference dbX = FirebaseDatabase.getInstance().getReference("repartidor/"+idRepartidor+"/pedidox/"+idPedido+"/");
                dbX.goOnline();
                dbX.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        verNota.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                AlertDialog.Builder mBuilder = new AlertDialog.Builder(seguimientoActivity.this);
                                View mV = getLayoutInflater().inflate(R.layout.dialog_direccion,null);
                                final EditText editDireccion = mV.findViewById(R.id.editDireccion);
                                Button aceptar = mV.findViewById(R.id.btnAceptarD);
                                Button cancelar = mV.findViewById(R.id.btnCancelarD);
                                TextView msg = mV.findViewById(R.id.textView12);
                                editDireccion.setText("");
                                msg.setText("Manda una nota al repartidor");
                                mBuilder.setView(mV);
                                final AlertDialog dialog = mBuilder.create();
                                dialog.show();
                                aceptar.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        DatabaseReference dbRefNota = FirebaseDatabase.getInstance().getReference("repartidor/"+idRepartidor+"/pedidox/").child(idPedido);
                                        dbRefNota.goOnline();

                                        Map<String, Object> datos = new HashMap<>();
                                        datos.put("notas",editDireccion.getText().toString());
                                        dbRefNota.updateChildren(datos);
                                        dbRefNota.goOffline();

                                        dialog.dismiss();
                                    }
                                });

                                cancelar.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                    }
                                });

                            }
                        });
                        repartidor datosRep = dataSnapshot.getValue(repartidor.class);
                        Double lat = datosRep.getLat();
                        Double lon = datosRep.getLon();
                        System.out.println("LAT LON"+lat+lon);
                        LatLng rep = new LatLng(lat,lon);
                        repartidor = mMap.addMarker(new MarkerOptions().position(rep).title("Repartidor").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                        notas.setText(dataSnapshot.child("notas").getValue().toString());
                        DatabaseReference dbRefNom = FirebaseDatabase.getInstance().getReference("repartidor/"+idRepartidor+"/").child("nombre");
                        dbRefNom.goOnline();

                        dbRefNom.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.getValue()!=null)
                                textNom.setText(dataSnapshot.getValue().toString());
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });

                        dbRefNom.goOffline();
                        DatabaseReference dbRefNotaYo = FirebaseDatabase.getInstance().getReference("usuario/"+idU+"/pedido/").child(idPedido);
                        dbRefNotaYo.goOnline();
                        dbRefNotaYo.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }

                        });
                        dbRefNotaYo.goOffline();
                        DatabaseReference dbRefConsulta = FirebaseDatabase.getInstance().getReference("repartidor/"+idRepartidor+"/pedidox/"+idPedido+"/");
                        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
                        dbRefConsulta.goOnline();
                        dbRefConsulta.addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                            }

                            @Override
                            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                repartidor.remove();

                                //posicionRepartidor();
                            }

                            @Override
                            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        dbRefConsulta.goOffline();

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                dbX.goOffline();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefUsuario.goOffline();

//
//        repartidor datosRep = dataSnapshot.getValue(repartidor.class);
//        Double lat=datosRep.getLat();
//        Double lon=datosRep.getLon();
//        LatLng rep = new LatLng(lat, lon);
//        repartidor = mMap.addMarker(new MarkerOptions().position(rep).title("Repartidor").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));


    }
    public void showNotification(Context context, String title, String message, Intent intent, int reqCode) {
        //SharedPreferenceManager sharedPreferenceManager = SharedPreferenceManager.getInstance(context);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, reqCode, intent, PendingIntent.FLAG_ONE_SHOT);
        String CHANNEL_ID = "channel_name";// The id of the channel.
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.mipmap.loma_foreground)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Channel Name";// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);
        }
        notificationManager.notify(reqCode, notificationBuilder.build()); // 0 is the request code, it should be unique id

        Log.d("showNotification", "showNotification: " + reqCode);
    }

}