package com.example.huevoapp;

public class repartidor {

    Double lat;
    Double lon;
    String idRepartidor;
    String nombre;
    String telefono;

    public repartidor(){

    }
    public repartidor(String idRepartidor, Double lat, Double lon, String nombre, String telefono) {
        this.lat = lat;
        this.lon = lon;
        this.idRepartidor=idRepartidor;
        this.nombre = nombre;
        this.telefono=telefono;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getIdRepartidor() {
        return idRepartidor;
    }

    public void setIdRepartidor(String idRepartidor) {
        this.idRepartidor = idRepartidor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}
