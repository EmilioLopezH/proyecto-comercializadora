package com.example.huevoapp.ui.notifications;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.huevoapp.MapsActivity;
import com.example.huevoapp.R;
import com.example.huevoapp.cargaImagen;
import com.example.huevoapp.pedidoActivity;
import com.example.huevoapp.sesionActivity;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.Context.CONTEXT_INCLUDE_CODE;

public class NotificationsFragment extends Fragment {
    private EditText textNombre, textDireccion, textTelefono;
    private TextView textHola, textCambiarD, textTerminos, textHorarios;
    private NotificationsViewModel notificationsViewModel;
    private Button textPedidos, textCerrarSesion;
    ArrayList<usuario> listaUsuario;
    ProgressDialog miprogreso;

    String idUsuario=null;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        notificationsViewModel =
                ViewModelProviders.of(this).get(NotificationsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_notifications, container, false);
      //  final TextView textView = root.findViewById(R.id.text_notifications);
        notificationsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
      //          textView.setText(s);
            }
        });
        textHola = root.findViewById(R.id.textHola);
        textNombre = root.findViewById(R.id.editNombre);
        textTelefono = root.findViewById(R.id.editTelefono);
        textDireccion = root.findViewById(R.id.editDireccion);
        textPedidos = root.findViewById(R.id.textPedidos);
        textCambiarD = root.findViewById(R.id.textCambiarD);
        textTerminos = root.findViewById(R.id.textTerminos);
        textHorarios = root.findViewById(R.id.textHorarios);
        textCerrarSesion = root.findViewById(R.id.CerrarSesion);
        miprogreso = new ProgressDialog(getActivity());
        idUsuario = (String) getActivity().getIntent().getSerializableExtra("idUsuario");
        // getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        ConnectivityManager connectivityManager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        if (networkInfo != null && networkInfo.isConnected()) {
            // Si hay conexión a Internet en este momento
        } else {
            new AlertDialog.Builder(getContext())
                    .setTitle("Error").setMessage("No es posible la conexión").setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().finish();
                }
            }).show();        }
        if(idUsuario.equals("nadie")){
            textHola.setText("Registrate para ver tu perfil");
            textPedidos.setVisibility(View.GONE);
            textCambiarD.setVisibility(View.GONE);
            textCerrarSesion.setText("REGISTRATE AQUI");
            textCerrarSesion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences preferences = getActivity().getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.clear().apply();
                    Intent i = new Intent(v.getContext(), sesionActivity.class);
                    startActivity(i);
                    getActivity().finish();
                }
            });
        }else{
            cargarUsuario(idUsuario);
            permisosUbicacion();

            textCerrarSesion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    android.app.AlertDialog.Builder mBuilder = new android.app.AlertDialog.Builder(v.getContext());
                    LayoutInflater li = (LayoutInflater) v.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View mV = li.inflate(R.layout.dialog_direccion,null);
                    EditText editDireccion = mV.findViewById(R.id.editDireccion);
                    editDireccion.setVisibility(View.GONE) ;
                    TextView texto = mV.findViewById(R.id.textView12);
                    texto.setText("¿Desea cerrar su sesión y salir?");
                    Button aceptar = mV.findViewById(R.id.btnAceptarD);
                    aceptar.setText("SI");
                    Button cancelar = mV.findViewById(R.id.btnCancelarD);
                    cancelar.setText("NO");
                    mBuilder.setView(mV);
                    final android.app.AlertDialog dialog = mBuilder.create();
                    aceptar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPreferences preferences = getActivity().getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.clear().apply();
                            dialog.dismiss();
                            getActivity().finish();
                        }
                    });
                    cancelar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });
                    // dialog.setCancelable(false);
                    // dialog.setCanceledOnTouchOutside(false);
                    dialog.show();
                }
            });
        }

        textCambiarD.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    //Button pressed
                    SpannableString content = new SpannableString("CAMBIAR DIRECCIÓN");
                    content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                    textCambiarD.setText(content);
                    return true;
                }
                else if (event.getAction() == MotionEvent.ACTION_UP) {
                    //Button released
                    textCambiarD.setText("CAMBIAR DIRECCIÓN");
                    miprogreso.setMessage("Espera...");
                    miprogreso.show();
                    Intent i = new Intent(getContext(), MapsActivity.class);
                    i.putExtra("idUsuario",idUsuario);
                    startActivity(i);
                    final Handler handler = new Handler();
                    final Runnable r = new Runnable() {
                        public void run() {

                            miprogreso.dismiss();
                            handler.postDelayed(this, 5000);
                        }
                    };
                    handler.postDelayed(r, 5000);
                    return true;
                }
                return false;
            }
        });

        textPedidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    Intent i = new Intent(getContext(), pedidoActivity.class);
                    i.putExtra("idUsuario",idUsuario);
                    startActivity(i);
            }
        });

        textTerminos.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    SpannableString content = new SpannableString("Términos y Condiciones");
                    content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                    textTerminos.setText(content);
                    return true;
                }
                else if (event.getAction() == MotionEvent.ACTION_UP) {
                    textTerminos.setText("Términos y Condiciones");
                   /* Intent i = new Intent(getContext(), pedidoActivity.class);
                    i.putExtra("idUsuario",idUsuario);
                    startActivity(i);*/
                    return true;
                }
                return false;
            }
        });

        textHorarios.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    SpannableString content = new SpannableString("Horarios y Cobertura");
                    content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
                    textHorarios.setText(content);
                    return true;
                }
                else if (event.getAction() == MotionEvent.ACTION_UP) {
                    textHorarios.setText("Horarios y Cobertura");
                    /*Intent i = new Intent(getContext(), pedidoActivity.class);
                    i.putExtra("idUsuario",idUsuario);
                    startActivity(i);*/
                    return true;
                }
                return false;
            }
        });

        return root;

    }

    private void cargarUsuario(String idU) {
        System.out.println("id de mi carga ususairo:"+idU);
        miprogreso.setMessage("Espera...");
        miprogreso.show();
        final DatabaseReference dbRefUsuario = FirebaseDatabase.getInstance().getReference("usuario/"+idU+"/");
        dbRefUsuario.goOnline();
        dbRefUsuario.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usuario datosUsuario = dataSnapshot.getValue(usuario.class);

                textHola.setText("Hola "+datosUsuario.getNombre());
                textNombre.setText(datosUsuario.getNombre());
                textDireccion.setText(datosUsuario.getDireccion());
                textTelefono.setText(datosUsuario.getTelefono());
                miprogreso.dismiss();
                }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefUsuario.goOffline();
    }

    private void permisosUbicacion(){
        try {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        CONTEXT_INCLUDE_CODE);
                return;
            }else{
                System.out.println("Accediendo...");
            }


        }catch (Exception e){
            System.out.println("Error "+e);
        }
    }
}
