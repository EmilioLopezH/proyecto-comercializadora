package com.example.huevoapp.ui.dashboard;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.example.huevoapp.R;
import com.example.huevoapp.productoActivity;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class adaptador extends RecyclerView.Adapter<adaptador.lista> {

    DecimalFormat formatter = new DecimalFormat("#,###.00");
    ArrayList<carrito> listaCarrito;

    public adaptador(ArrayList<carrito> listaCarrito) {
        this.listaCarrito = listaCarrito;
    }

    @NonNull
    @Override
    public lista onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_dashboard,parent,false);
        return new lista(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final lista holder, final int position) {
final String idCarrito = listaCarrito.get(position).getIdCarrito();
final String idUsuario = listaCarrito.get(position).getIdUsuario();
        holder.textoTipo.setText(listaCarrito.get(position).getTipo());
        holder.textoCantidad.setText("Cantidad: "+listaCarrito.get(position).getCantidad());
        holder.textoPrecioTot.setText("Total: $ "+formatter.format(Double.parseDouble(listaCarrito.get(position).getPrecioTotal()))+" MXN");
        holder.btnEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(v.getContext() , "Boton para el producto: "+listaCarrito.get(position).getTipo(), Toast.LENGTH_LONG).show();
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(v.getContext());
                LayoutInflater li = (LayoutInflater) v.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mV = li.inflate(R.layout.dialog_direccion,null);
                EditText editDireccion = mV.findViewById(R.id.editDireccion);
                editDireccion.setVisibility(View.GONE) ;
                TextView texto = mV.findViewById(R.id.textView12);
                texto.setText("¿Desea eliminar este item del carrito?");
                Button aceptar = mV.findViewById(R.id.btnAceptarD);
                aceptar.setText("SI");
                Button cancelar = mV.findViewById(R.id.btnCancelarD);
                cancelar.setText("NO");
                mBuilder.setView(mV);
                final AlertDialog dialog = mBuilder.create();
                aceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DatabaseReference dbRefEliminar;
                        dbRefEliminar = FirebaseDatabase.getInstance().getReference("usuario/"+idUsuario+"/carrito/producto/");
                        dbRefEliminar.goOnline();
                        dbRefEliminar.child(idCarrito).removeValue();
                        dbRefEliminar.goOffline();
                        dialog.dismiss();
                    }
                });
                cancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });


               // dialog.setCancelable(false);
               // dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        });

        final StorageReference mImageRef =
                FirebaseStorage.getInstance().getReference("/imagen/producto/"+listaCarrito.get(position).getIdProducto()+".jpg");
        System.out.println("PATH:          "+mImageRef.getPath());

        final long ONE_MEGABYTE = 1024 * 1024;
        mImageRef.getBytes(ONE_MEGABYTE)
                .addOnSuccessListener(new OnSuccessListener<byte[]>() {
                    @Override
                    public void onSuccess(byte[] bytes) {
                        Bitmap bm = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        holder.imagen.setImageBitmap(bm);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return listaCarrito.size();
    }

    public class lista extends RecyclerView.ViewHolder {
    TextView textoCantidad,textoTipo,textoPrecioTot;
    Button btnEliminar;
    ImageView imagen;
        public lista(@NonNull View itemView) {
            super(itemView);
            textoTipo = (TextView) itemView.findViewById(R.id.textTipo);
            textoCantidad = (TextView) itemView.findViewById(R.id.textCantidad);
           textoPrecioTot = (TextView) itemView.findViewById(R.id.textPrecioTot);
          btnEliminar = (Button) itemView.findViewById(R.id.btnEliminar);
          imagen = (ImageView) itemView.findViewById(R.id.imageCarrito);
        }
    }
}
