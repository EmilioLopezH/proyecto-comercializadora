package com.example.huevoapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.huevoapp.ui.pedidoY.pedidoY;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class verpedidoActivity extends AppCompatActivity {
String idUsuario=null, idPedido=null, idRepartidor=null;
    ProgressDialog miprogreso;

    private TextView textInicio, textDirecc, textLista, textPrecio;
    Button cancelar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_verpedido);
        idUsuario = (String) getIntent().getSerializableExtra("idUsuario");
        idPedido = (String) getIntent().getSerializableExtra("idPedido");
        idRepartidor = (String) getIntent().getSerializableExtra("idRepartidor");
        miprogreso = new ProgressDialog(this);
        textInicio = findViewById(R.id.datosVer);
        textDirecc = findViewById(R.id.direccVer);
        textLista = findViewById(R.id.desCompraVer);
        textPrecio = findViewById(R.id.textPreciosVer);
        cancelar = findViewById(R.id.btnCancelarVer);
        System.out.println(idUsuario+idPedido);

        if(idUsuario!=null)
            cargarPedidosUsuario(idUsuario, idPedido);
        else if(idRepartidor!=null)
            cargarPedidosRepartidor(idRepartidor, idPedido);

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    void cargarPedidosUsuario(String idU, String idP){
        miprogreso.setMessage("Espera...");
        miprogreso.show();
        final DatabaseReference dbRefPedido = FirebaseDatabase.getInstance().getReference("usuario/"+idU+"/pedidoy/"+idP+"/"); //+idU+"/carrito/producto
        dbRefPedido.goOnline();
        dbRefPedido.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                pedidoY datosPedido = dataSnapshot.getValue(pedidoY.class);

                textInicio.setText(datosPedido.getFolio()+"\n"+datosPedido.getFecha());
                textDirecc.setText(datosPedido.getDireccion());
                textLista.setText(datosPedido.getProductos());
                System.out.println(datosPedido.getProductos());
                System.out.println(datosPedido.getPrecios());
                textPrecio.setText(datosPedido.getPrecios());
                miprogreso.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefPedido.goOffline();
    }

    void cargarPedidosRepartidor(String idR, String idP){
        System.out.println(idR+idP);
        miprogreso.setMessage("Espera...");
        miprogreso.show();
        final DatabaseReference dbRefPedido = FirebaseDatabase.getInstance().getReference("repartidor/"+idR+"/pedidoy/"+idP+"/"); //+idU+"/carrito/producto
        dbRefPedido.goOnline();
        dbRefPedido.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                pedidoY datosPedido = dataSnapshot.getValue(pedidoY.class);

                textInicio.setText(datosPedido.getFolio()+"\n"+datosPedido.getFecha());
                textDirecc.setText(datosPedido.getDireccion());
                textLista.setText(datosPedido.getProductos());
                System.out.println(datosPedido.getProductos());
                System.out.println(datosPedido.getPrecios());
                textPrecio.setText(datosPedido.getPrecios());
                miprogreso.dismiss();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefPedido.goOffline();
    }
}
