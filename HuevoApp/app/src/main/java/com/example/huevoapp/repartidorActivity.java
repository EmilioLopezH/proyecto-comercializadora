package com.example.huevoapp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.huevoapp.ui.notifications.usuario;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;

import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class repartidorActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private FusedLocationProviderClient location;
    private Location geoLocation;
    private ResultReceiver resultReceiver;
    Marker miMarcador;
    Marker pedidoMarker=null;
    String nombre, telefono, compraLista, recibo, total;
    String miDireccion="";
    String idUsuario=null;
    String idPedido=null;
    String idRepartidor=null;
    Double precios=0.0;
    TextView textAddress, textNotas, textNotasYo;
    Double lat, lon;
    ProgressDialog miprogreso;
    Button btnPedido, btnPedido2;
    boolean activo=true;
    private DatabaseReference dbRefR, dbRefR2,dbRefR3,dbRefR4,dbRefR5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        System.out.println("welcome");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_repartidor);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapR);
        mapFragment.getMapAsync(this);

        resultReceiver = new AddressResultReceiver(new Handler());
        textAddress = findViewById(R.id.textDireccionPedido);
        final DecimalFormat formatter = new DecimalFormat("#,###.00");
        btnPedido = findViewById(R.id.btnVerPedido);
        btnPedido2 = findViewById(R.id.btnVerPedido2);
        idPedido = (String) getIntent().getSerializableExtra("idPedido");
        idRepartidor = (String) getIntent().getSerializableExtra("idRepartidor");
        textNotas = findViewById(R.id.textNota);
        textNotasYo = findViewById(R.id.textNotaYo);

        btnPedido2.setVisibility(View.GONE);
        textNotas.setVisibility(View.GONE);
        textNotasYo.setVisibility(View.GONE);


        buscaRepartidor();


        btnPedido.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println(compraLista+total+nombre+telefono);
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(repartidorActivity.this);
                View mV = getLayoutInflater().inflate(R.layout.dialog_pedido,null);
                Button aceptar = mV.findViewById(R.id.btnAceptarCliente);
                Button regresar = mV.findViewById(R.id.btnRegresarCliente);
                Button cancelar = mV.findViewById(R.id.btnCancelarCliente);

                EditText nom = mV.findViewById(R.id.nombreClienteEdit);
                nom.setText(nombre);
                EditText tele = mV.findViewById(R.id.telefonoClienteEdit);
                tele.setText(telefono);
                EditText lista = mV.findViewById(R.id.productosClienteEdit);
                lista.setText(compraLista);
                EditText tot = mV.findViewById(R.id.totalClienteEdit);
                tot.setText("$ "+formatter.format(Double.parseDouble(total))+" MXN");
                mBuilder.setView(mV);
                final AlertDialog dialog = mBuilder.create();
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
                aceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //confirmar pedido
                        AlertDialog.Builder mBuilder = new AlertDialog.Builder(v.getContext());
                        LayoutInflater li = (LayoutInflater) v.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View mV = li.inflate(R.layout.dialog_direccion,null);
                        EditText editDireccion = mV.findViewById(R.id.editDireccion);
                        editDireccion.setVisibility(View.GONE) ;
                        TextView texto = mV.findViewById(R.id.textView12);
                        texto.setText("¿Desea confirmar y entregar este pedido?");
                        Button aceptar = mV.findViewById(R.id.btnAceptarD);
                        aceptar.setText("SI");
                        Button cancelar = mV.findViewById(R.id.btnCancelarD);
                        cancelar.setText("NO");
                        mBuilder.setView(mV);
                        final AlertDialog dialogx = mBuilder.create();
                        dialogx.setCanceledOnTouchOutside(false);
                        dialogx.show();
                        aceptar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //si aceptar pedido
                                aceptarPedido(idPedido,idRepartidor);
                                dialogx.dismiss();
                                dialog.dismiss();
                                finish();

                            }
                        });
                        cancelar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogx.dismiss();
                                dialog.dismiss();
                            }
                        });
                    }
                });

                regresar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                cancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                            //cancelar pedido
                        AlertDialog.Builder mBuilder = new AlertDialog.Builder(v.getContext());
                        LayoutInflater li = (LayoutInflater) v.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View mV = li.inflate(R.layout.dialog_direccion,null);
                        EditText editDireccion = mV.findViewById(R.id.editDireccion);
                        editDireccion.setVisibility(View.GONE) ;
                        TextView texto = mV.findViewById(R.id.textView12);
                        texto.setText("¿Desea CANCELAR este pedido?");
                        Button aceptar = mV.findViewById(R.id.btnAceptarD);
                        aceptar.setText("SI");
                        Button cancelar = mV.findViewById(R.id.btnCancelarD);
                        cancelar.setText("NO");
                        mBuilder.setView(mV);
                        final AlertDialog dialogy = mBuilder.create();
                        dialogy.setCanceledOnTouchOutside(false);
                        dialogy.show();
                        aceptar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //si cancelar pedido
                                dialogy.dismiss();
                                dialog.dismiss();
                                finish();
                            }
                        });
                        cancelar.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialogy.dismiss();
                                dialog.dismiss();
                            }
                        });
                    }
                });
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.getUiSettings().setZoomControlsEnabled(true);

        // Add a marker in Sydney and move the camera
        // LatLng morelia = new LatLng(19.7027116, -101.1923818);
        // mMap.addMarker(new MarkerOptions().position(morelia).title("Estas en Morelia, Mich."));
        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(morelia, 10f));

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(this, "No cuenta con los permisos", Toast.LENGTH_LONG).show();
            return;
        }

        tuUbicacion();

        final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {

                actualizarUbicacion(geoLocation);
                System.out.println("Actualizar...............");
                handler.postDelayed(this, 25000);
            }
        };

        handler.postDelayed(r, 25000);

    }


    private void fetchAddressFromLatLon(Location l) {
        Intent intent = new Intent(this, buscaDireccion.class);
        intent.putExtra(Constants.RECEIVER, resultReceiver);
        intent.putExtra(Constants.LOCATION_DATA_EXTRA, l);
        startService(intent);

    }

    private class AddressResultReceiver extends ResultReceiver {
        AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);
            if (resultCode == Constants.SUCCESS_RESULT) {
                miDireccion = resultData.getString(Constants.RESULT_DATA_KEY);
                textAddress.setText(resultData.getString(Constants.RESULT_DATA_KEY));
            } else {
                System.out.println("Valio madres " + resultData.getString(Constants.RESULT_DATA_KEY));
            }
        }
    }

    private void tuUbicacion() {
        location = LocationServices.getFusedLocationProviderClient(this);
        System.out.println("Location location"+ location);
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);

        location.getLastLocation().addOnSuccessListener(this, new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    geoLocation = location;
                    lat=location.getLatitude();
                    lon=location.getLongitude();
                    LatLng mi = new LatLng(lat, lon);
                    miMarcador = mMap.addMarker(new MarkerOptions().position(mi).title("Tu estás aqui").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
                    //mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,10f));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mi,12f));
                    //mandarUbicacionAUsuario();
                   /* Location lo = new Location("providerNA");
                    lo.setLatitude(lat);
                    lo.setLongitude(lon);
                    fetchAddressFromLatLon(lo);*/
                } else {
                    System.out.println("error");
                }
            }
        });
    }

    private void actualizarUbicacion(Location loc){
        double lat, lon;
        lat=loc.getLatitude();
        lon=loc.getLongitude();
        LatLng mi = new LatLng(lat, lon);
        miMarcador.remove();
        miMarcador = mMap.addMarker(new MarkerOptions().position(mi).title("Tu estás aqui").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney,10f));
        //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(mi,16f));
        // mandarUbicacionAUsuario();
       /*DatabaseReference dbRefActualiza = FirebaseDatabase.getInstance().getReference("repartidor/ubicacion/");
        dbRefActualiza.goOnline();
        Map<String, Object> datos = new HashMap<>();
        datos.put("lat",lat);
        datos.put("lon",lon);
        dbRefActualiza.updateChildren(datos);
        dbRefActualiza.goOffline();*/
        /*Location lo = new Location("providerNA");
        lo.setLatitude(lat);
        lo.setLongitude(lon);
        fetchAddressFromLatLon(lo);*/
    }

    private void aceptarPedido(String idP, String idR){
        Toast.makeText(this, ""+idP+" : "+idR+" : "+idUsuario, Toast.LENGTH_LONG).show();
       /* */
        dbRefR = FirebaseDatabase.getInstance().getReference("usuario/"+idUsuario+"/pedido/").child(idPedido);
        dbRefR.goOnline();
        Map<String, Object> datos = new HashMap<>();
        datos.put("estado","activo");
        datos.put("idRepartidor",idRepartidor);
        dbRefR.updateChildren(datos);
        dbRefR.goOffline();

        dbRefR5 = FirebaseDatabase.getInstance().getReference("usuario/").child(idUsuario);
        dbRefR5.goOnline();
        Map<String, Object> datos2 = new HashMap<>();
        datos.put("notificacion","activo");
        dbRefR5.updateChildren(datos2);
        dbRefR5.goOffline();


        dbRefR2 = FirebaseDatabase.getInstance().getReference("repartidor/"+idRepartidor+"/pedidox/"+idPedido+"/");
        dbRefR3 = FirebaseDatabase.getInstance().getReference("repartidor/pedido/"+idPedido+"/");
        mueveChild(dbRefR3,dbRefR2);

        activo=false;
        finish();
    }

    private void buscaRepartidor(){
        DatabaseReference dbRefUsuario;

         dbRefUsuario = FirebaseDatabase.getInstance().getReference("repartidor/pedido/"+idPedido);

        dbRefUsuario.goOnline();
        dbRefUsuario.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(activo){
                textAddress.setText(dataSnapshot.child("direccion").getValue().toString());
                idUsuario=dataSnapshot.child("idUsuario").getValue().toString();}
                DatabaseReference dbX = FirebaseDatabase.getInstance().getReference("usuario/"+idUsuario+"/pedido/"+idPedido);
                dbX.goOnline();
                dbX.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        Double lat=Double.parseDouble(dataSnapshot.child("lat").getValue().toString());
                        Double lon=Double.parseDouble(dataSnapshot.child("lon").getValue().toString());
                        compraLista=dataSnapshot.child("productos").getValue().toString();
                        total=dataSnapshot.child("total").getValue().toString();

                        DatabaseReference dbUS = FirebaseDatabase.getInstance().getReference("usuario/"+idUsuario);
                        dbUS.goOnline();
                        dbUS.addListenerForSingleValueEvent(new ValueEventListener() {
                                                      @Override
                                                      public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                          nombre=dataSnapshot.child("nombre").getValue().toString();
                                                          telefono=dataSnapshot.child("telefono").getValue().toString();
                                                      }

                                                      @Override
                                                      public void onCancelled(@NonNull DatabaseError databaseError) {

                                                      }
                                                  }
                        );
                        dbUS.goOnline();

                                LatLng rep = new LatLng(lat, lon);
                        pedidoMarker = mMap.addMarker(new MarkerOptions().position(rep).title("Entrega Aqui").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
                        DatabaseReference dbRefConsulta = FirebaseDatabase.getInstance().getReference("usuario/"+idUsuario+"/pedido/"+idPedido+"/");
                        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
                        dbRefConsulta.goOnline();
                        dbRefConsulta.addChildEventListener(new ChildEventListener() {
                            @Override
                            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                            }

                            @Override
                            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                pedidoMarker.remove();

                                //posicionRepartidor();
                            }

                            @Override
                            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                            }

                            @Override
                            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                        dbRefConsulta.goOffline();
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
                dbX.goOffline();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefUsuario.goOffline();

//
//        repartidor datosRep = dataSnapshot.getValue(repartidor.class);
//        Double lat=datosRep.getLat();
//        Double lon=datosRep.getLon();
//        LatLng rep = new LatLng(lat, lon);
//        repartidor = mMap.addMarker(new MarkerOptions().position(rep).title("Repartidor").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));


    }
    private void mueveChild(final DatabaseReference fromPath, final DatabaseReference toPath) {
        fromPath.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                toPath.setValue(dataSnapshot.getValue(), new DatabaseReference.CompletionListener() {
                    @Override
                    public void onComplete(DatabaseError firebaseError, DatabaseReference firebase) {
                        if (firebaseError != null) {
                            System.out.println("Copy failed");
                        } else {
                            System.out.println("Success");
                            dbRefR4 = FirebaseDatabase.getInstance().getReference("repartidor/pedido/");
                            dbRefR4.goOnline();
                            dbRefR4.child(idPedido).removeValue();
                            dbRefR4.goOffline();

                        }
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


}