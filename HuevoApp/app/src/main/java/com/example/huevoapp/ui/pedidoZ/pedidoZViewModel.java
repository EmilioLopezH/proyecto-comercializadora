package com.example.huevoapp.ui.pedidoZ;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class pedidoZViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public pedidoZViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is a fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
