package com.example.huevoapp.ui.pedidoX;

import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.huevoapp.MainActivity;
import com.example.huevoapp.R;
import com.example.huevoapp.pedidoActivity;
import com.example.huevoapp.seguimientoActivity;

import java.text.DecimalFormat;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.recyclerview.widget.RecyclerView;

public class adaptadorPedidoX extends RecyclerView.Adapter<adaptadorPedidoX.lista> {
    DecimalFormat formatter = new DecimalFormat("#,###.00");
    ArrayList<pedidoX> listaPedido;

    public adaptadorPedidoX(ArrayList<pedidoX> listaPedido) {
        this.listaPedido = listaPedido;
    }

    @NonNull
    @Override
    public adaptadorPedidoX.lista onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_pedidox,parent,false);
        return new lista(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final adaptadorPedidoX.lista holder, final int position) {
        final String folio = listaPedido.get(position).getFolio();
        final String fecha=listaPedido.get(position).getFecha();
        final String direcc = listaPedido.get(position).getDireccion();
        final Double precio=listaPedido.get(position).getTotal();
        final String idUsuario=listaPedido.get(position).getIdUsuario();
        System.out.println("BUENOS DIAS");

        holder.textFolio.setText(folio);
        holder.textFecha.setText(fecha);
        holder.textDireccion.setText(direcc);
        holder.textPrecio.setText("$ "+formatter.format(Double.parseDouble(precio.toString()))+" MXN");

        if(listaPedido.get(position).getEstado().contains("falso")){
            holder.btnEstado.setTextColor(Color.RED);
            holder.btnEstado.setText("Esperando repartidor...");
            holder.btnEstado.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext() , "Espera a que tu pedido sea aceptado por un repartidor", Toast.LENGTH_LONG).show();
                }});
        }
        if(listaPedido.get(position).getEstado().contains("activo")){
            holder.btnEstado.setTextColor(Color.rgb(76,175,80));
            holder.btnEstado.setText("Ver Seguimiento");
            holder.btnEstado.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(v.getContext(), seguimientoActivity.class);
                    i.putExtra("idPedido",folio);
                    i.putExtra("idUsuario",idUsuario);
                    v.getContext().startActivity(i);
                }});
        }
        if(listaPedido.get(position).getEstado().contains("cancelado")){
            holder.btnEstado.setTextColor(Color.rgb(76,175,80));
            holder.btnEstado.setText("TU PEDIDO A SIDO CANCELADO");
            holder.btnEstado.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(v.getContext() , "Contáctanos para mayor información 4431257805", Toast.LENGTH_LONG).show();
                }});
        }
    }

    @Override
    public int getItemCount() {
        return listaPedido.size();
    }

    public class lista extends RecyclerView.ViewHolder {
        TextView textFolio, textFecha, textPrecio, textDireccion;
        Button btnEstado;
        public lista(@NonNull View itemView) {
            super(itemView);
            textFolio = (TextView) itemView.findViewById(R.id.textFolio);
            textFecha = (TextView) itemView.findViewById(R.id.textFecha);
            textPrecio = (TextView) itemView.findViewById(R.id.textTotalPedido);
            textDireccion = (TextView) itemView.findViewById(R.id.textDireccP);
            btnEstado = itemView.findViewById(R.id.btnSeguimiento);
        }
    }
    public void showNotification(Context context, String title, String message, Intent intent, int reqCode) {
        //SharedPreferenceManager sharedPreferenceManager = SharedPreferenceManager.getInstance(context);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, reqCode, intent, PendingIntent.FLAG_ONE_SHOT);
        String CHANNEL_ID = "channel_name";// The id of the channel.
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.mipmap.loma_foreground)
                .setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .setContentIntent(pendingIntent);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Channel Name";// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            notificationManager.createNotificationChannel(mChannel);
        }
        notificationManager.notify(reqCode, notificationBuilder.build()); // 0 is the request code, it should be unique id

        Log.d("showNotification", "showNotification: " + reqCode);
    }
}
