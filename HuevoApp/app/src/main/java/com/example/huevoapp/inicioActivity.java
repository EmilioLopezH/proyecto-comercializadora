package com.example.huevoapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

public class inicioActivity extends AppCompatActivity {
Button boton;
String id=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_inicio);

        boton = findViewById(R.id.button);
        boton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cargarPreferencias();

            }
        });

        verificarPermisos();


    }
    private void cargarPreferencias(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String usuario=preferences.getString("usuario",null);
        String repartidor=preferences.getString("repartidor",null);
        String admin=preferences.getString("admin",null);
        if(usuario!=null || repartidor!=null || admin!=null){
            if(usuario!=null){
                id=usuario;
                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                i.putExtra("idUsuario",id);
                startActivity(i);
                finish();
            }else if(repartidor!=null){
                id=repartidor;
                Intent i = new Intent(getApplicationContext(),repartidorMainActivity.class);
                i.putExtra("idRepartidor",id);
                startActivity(i);
                finish();
            }else{
                id=admin;
               /* Intent i = new Intent(getApplicationContext(),repartidorMainActivity.class);
                i.putExtra("idAdmin",id);
                startActivity(i);
                finish();*/
            }
        }else{
            id=null;
            Intent i = new Intent(getApplicationContext(),sesionActivity.class);
            startActivity(i);
            finish();
        }

    }
    private void verificarPermisos(){
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        CONTEXT_INCLUDE_CODE);
                return;
            }else{
                System.out.println("Accediendo...");
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        CONTEXT_INCLUDE_CODE);
                return;
            }else{
                System.out.println("Accediendo...");
            }


        }catch (Exception e){
            System.out.println("Error "+e);
        }
    }
}
