package com.example.huevoapp.ui.pedidoZ;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.huevoapp.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class pedidoZFragment extends Fragment {
    private pedidoZViewModel pedidoYViewModel;
    String idUsuario=null, idRepartidor=null;
    ProgressDialog miprogreso;
    ArrayList<pedidoZ> listaPedido;
    RecyclerView recyclerPedido;
    Boolean b = null;
    Button regresar, salir;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        pedidoYViewModel = ViewModelProviders.of(this).get(pedidoZViewModel.class);
        View root = inflater.inflate(R.layout.fragment_pedidoz, container, false);
        // final TextView textView = root.findViewById(R.id.text_home);
        pedidoYViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });
        miprogreso = new ProgressDialog(getActivity());
        idUsuario = (String) getActivity().getIntent().getSerializableExtra("idUsuario");
        idRepartidor = (String) getActivity().getIntent().getSerializableExtra("idRepartidor");
        System.out.println("en Z "+idRepartidor);
        System.out.println("en Z "+idUsuario);
        listaPedido = new ArrayList<>();
        recyclerPedido = root.findViewById(R.id.recyclerPedidoZ);
        recyclerPedido.setLayoutManager(new LinearLayoutManager(getContext()));
        regresar = root.findViewById(R.id.btnRegresarZ);
        salir = root.findViewById(R.id.btnRegresarZ2);
        salir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.app.AlertDialog.Builder mBuilder = new android.app.AlertDialog.Builder(v.getContext());
                LayoutInflater li = (LayoutInflater) v.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View mV = li.inflate(R.layout.dialog_direccion,null);
                EditText editDireccion = mV.findViewById(R.id.editDireccion);
                editDireccion.setVisibility(View.GONE) ;
                TextView texto = mV.findViewById(R.id.textView12);
                texto.setText("¿Desea cerrar su sesión y salir?");
                Button aceptar = mV.findViewById(R.id.btnAceptarD);
                aceptar.setText("SI");
                Button cancelar = mV.findViewById(R.id.btnCancelarD);
                cancelar.setText("NO");
                mBuilder.setView(mV);
                final android.app.AlertDialog dialog = mBuilder.create();
                aceptar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        SharedPreferences preferences = getActivity().getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.clear().apply();
                        dialog.dismiss();
                        getActivity().finish();
                    }
                });
                cancelar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                // dialog.setCancelable(false);
                // dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        });
        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        DatabaseReference dbRefConsulta = FirebaseDatabase.getInstance().getReference("repartidor/"+idRepartidor+"/pedidox/");
        dbRefConsulta.goOnline();
        dbRefConsulta.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                listaPedido.clear();
                recyclerPedido.setAdapter(null);
                recyclerPedido.setLayoutManager(new LinearLayoutManager(getActivity()));
            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                listaPedido.clear();
                recyclerPedido.setAdapter(null);
                recyclerPedido.setLayoutManager(new LinearLayoutManager(getActivity()));

            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                listaPedido.clear();
                recyclerPedido.setAdapter(null);
                recyclerPedido.setLayoutManager(new LinearLayoutManager(getActivity()));
            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefConsulta.goOffline();


        cargarPedidos(idRepartidor);
        return root;
    }
    private void cargarPedidos(final String idR){

        final DatabaseReference dbRefPedido = FirebaseDatabase.getInstance().getReference("repartidor/"+idR+"/pedidox/");
        dbRefPedido.goOnline();
        dbRefPedido.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot : dataSnapshot.getChildren()){
                    dbRefPedido.child(snapshot.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            pedidoZ datosPedido = snapshot.getValue(pedidoZ.class);
                            listaPedido.add(new pedidoZ(datosPedido.getFecha(), datosPedido.getDireccion(),datosPedido.getLat(),datosPedido.getLon(),datosPedido.getPrecios(),datosPedido.getProductos(),datosPedido.getTotal(),datosPedido.getFolio(),datosPedido.getEstado(),datosPedido.getIdUsuario(),idR));
                            adaptadorPedidoZ adaptadorZ = new adaptadorPedidoZ(listaPedido);
                            recyclerPedido.setAdapter(adaptadorZ);
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        dbRefPedido.goOffline();

    }
}
