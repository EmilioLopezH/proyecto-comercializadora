package com.example.huevoapp.ui.pedidoW;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class pedidoWViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public pedidoWViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is a fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
