package com.example.huevoapp.ui.pedidoX;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.huevoapp.MainActivity;
import com.example.huevoapp.R;

import java.util.ArrayList;

import com.example.huevoapp.pedidoActivity;
import com.example.huevoapp.seguimientoActivity;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class pedidoXFragment extends Fragment {
    private pedidoXViewModel pedidoXViewModel;
    String idUsuario=null;
    String idRepartidor=null;
    ProgressDialog miprogreso;
    ArrayList<pedidoX> listaPedido;
    RecyclerView recyclerPedido;
    Boolean b = false;
    Button regresar;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        pedidoXViewModel = ViewModelProviders.of(this).get(pedidoXViewModel.class);
        View root = inflater.inflate(R.layout.fragment_pedidox, container, false);
        // final TextView textView = root.findViewById(R.id.text_home);
        pedidoXViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });
        miprogreso = new ProgressDialog(getActivity());
        idUsuario = (String) getActivity().getIntent().getSerializableExtra("idUsuario");
        idRepartidor = (String) getActivity().getIntent().getSerializableExtra("idRepartidor");
        listaPedido = new ArrayList<>();
        recyclerPedido = root.findViewById(R.id.recyclerPedidoX);
        recyclerPedido.setLayoutManager(new LinearLayoutManager(getContext()));
        regresar = root.findViewById(R.id.btnRegresar);
        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        cargarPedidos(idUsuario);
        System.out.println("mi boleano: "+b);
        System.out.println("mi ide ususaio: "+idUsuario);
        System.out.println("mi ide repartidor"+idRepartidor);
        /*final Handler handler = new Handler();
        final Runnable r = new Runnable() {
            public void run() {
                actualizar(idUsuario);
                handler.postDelayed(this, 3000);
            }
        };
        handler.postDelayed(r, 3000);*/
        DatabaseReference dbRefConsulta = FirebaseDatabase.getInstance().getReference("usuario/"+idUsuario+"/pedido/");
        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
        dbRefConsulta.goOnline();
        dbRefConsulta.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                listaPedido.clear();
                recyclerPedido.setAdapter(null);
                recyclerPedido.setLayoutManager(new LinearLayoutManager(getActivity()));

                //cargarPedidos(idUsuario);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefConsulta.goOffline();

        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        return root;
    }

    private void cargarPedidos(final String idU){
       // miprogreso.setMessage("Espera...");
       // miprogreso.show();
        final DatabaseReference dbRefPedido = FirebaseDatabase.getInstance().getReference("usuario/"+idU+"/pedido/");
        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
        dbRefPedido.goOnline();
        dbRefPedido.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot : dataSnapshot.getChildren()){
                    dbRefPedido.child(snapshot.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            pedidoX datosPedido = snapshot.getValue(pedidoX.class);
                            listaPedido.add(new pedidoX(datosPedido.getFecha(), datosPedido.getDireccion(),datosPedido.getLat(),datosPedido.getLon(),datosPedido.getListaPrecio(),datosPedido.getListaProduto(),datosPedido.getTotal(),datosPedido.getFolio(),datosPedido.getEstado(),idU,null,getContext()));
                            adaptadorPedidoX adaptadorX = new adaptadorPedidoX(listaPedido);
                            recyclerPedido.setAdapter(adaptadorX);

                         //   miprogreso.dismiss();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        dbRefPedido.goOffline();

    }

    private void actualizar(final String idU){
        listaPedido.clear();
        cargarPedidos(idU);
    }


}
