package com.example.huevoapp.ui.pedidoY;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.huevoapp.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class pedidoYFragment extends Fragment {
    private pedidoYViewModel pedidoYViewModel;
    String idUsuario=null, idRepartidor=null;
    ProgressDialog miprogreso;
    ArrayList<pedidoY> listaPedido;
    RecyclerView recyclerPedido;
    Boolean b = null;
    Button regresar;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        pedidoYViewModel = ViewModelProviders.of(this).get(pedidoYViewModel.class);
        View root = inflater.inflate(R.layout.fragment_pedidoy, container, false);
        // final TextView textView = root.findViewById(R.id.text_home);
        pedidoYViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
            }
        });
        miprogreso = new ProgressDialog(getActivity());
        idUsuario = (String) getActivity().getIntent().getSerializableExtra("idUsuario");
        idRepartidor = (String) getActivity().getIntent().getSerializableExtra("idRepartidor");
        listaPedido = new ArrayList<>();
        recyclerPedido = root.findViewById(R.id.recyclerPedidoY);
        recyclerPedido.setLayoutManager(new LinearLayoutManager(getContext()));
        regresar = root.findViewById(R.id.btnRegresarY);
        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        System.out.println(idRepartidor+" "+idUsuario);



        DatabaseReference dbRefConsulta = FirebaseDatabase.getInstance().getReference("usuario/"+idUsuario+"/pedidoy/");
        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
        dbRefConsulta.goOnline();
        dbRefConsulta.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                listaPedido.clear();
                recyclerPedido.setAdapter(null);
                recyclerPedido.setLayoutManager(new LinearLayoutManager(getActivity()));

                //cargarPedidos(idUsuario);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefConsulta.goOffline();
        DatabaseReference dbRefConsulta2 = FirebaseDatabase.getInstance().getReference("repartidor/"+idRepartidor+"/pedidoy/");
        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
        dbRefConsulta2.goOnline();
        dbRefConsulta2.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                listaPedido.clear();
                recyclerPedido.setAdapter(null);
                recyclerPedido.setLayoutManager(new LinearLayoutManager(getActivity()));

                //cargarPedidos(idUsuario);
            }

            @Override
            public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefConsulta2.goOffline();


        if(idUsuario!=null)
        cargarPedidosUsuario(idUsuario);
        else if(idRepartidor!=null)
        cargarPedidosRepartidor(idRepartidor);


        return root;
    }
    private void cargarPedidosUsuario(final String idU){

        final DatabaseReference dbRefPedido = FirebaseDatabase.getInstance().getReference("usuario/"+idU+"/pedidoy/");
        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
        dbRefPedido.goOnline();
        dbRefPedido.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot : dataSnapshot.getChildren()){
                    dbRefPedido.child(snapshot.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            pedidoY datosPedido = snapshot.getValue(pedidoY.class);
                            listaPedido.add(new pedidoY(datosPedido.getFecha(), datosPedido.getDireccion(),datosPedido.getLat(),datosPedido.getLon(),datosPedido.getPrecios(),datosPedido.getProductos(),datosPedido.getTotal(),datosPedido.getFolio(),datosPedido.getEstado(),idU,null));
                            adaptadorPedidoY adaptadorY = new adaptadorPedidoY(listaPedido);
                            recyclerPedido.setAdapter(adaptadorY);
                            //   miprogreso.dismiss();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        dbRefPedido.goOffline();
        // miprogreso.setMessage("Espera...");
        // miprogreso.show();


    }

    private void cargarPedidosRepartidor(final String idR){

        final DatabaseReference dbRefPedido = FirebaseDatabase.getInstance().getReference("repartidor/"+idR+"/pedidoy/");
        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
        dbRefPedido.goOnline();
        dbRefPedido.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot : dataSnapshot.getChildren()){
                    dbRefPedido.child(snapshot.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            pedidoY datosPedido = snapshot.getValue(pedidoY.class);
                            listaPedido.add(new pedidoY(datosPedido.getFecha(), datosPedido.getDireccion(),datosPedido.getLat(),datosPedido.getLon(),datosPedido.getPrecios(),datosPedido.getProductos(),datosPedido.getTotal(),datosPedido.getFolio(),datosPedido.getEstado(),null,idR));
                            adaptadorPedidoY adaptadorY = new adaptadorPedidoY(listaPedido);
                            recyclerPedido.setAdapter(adaptadorY);
                            //   miprogreso.dismiss();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        dbRefPedido.goOffline();
    }
}
