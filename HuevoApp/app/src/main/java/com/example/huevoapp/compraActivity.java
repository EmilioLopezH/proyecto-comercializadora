package com.example.huevoapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.huevoapp.ui.dashboard.adaptador;
import com.example.huevoapp.ui.dashboard.carrito;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class compraActivity extends AppCompatActivity {
String compraLista="";
String idUsuario="null";
    carrito datosCarrito;
Double total=0.0;
    long count=0;
    long dataSnap = 0;
    Button confirmar, cancelar;
TextView textCompra, textPrecios;
    ProgressDialog miprogreso;
DecimalFormat formatter = new DecimalFormat("#,###.00");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_compra);
        idUsuario = (String) getIntent().getSerializableExtra("idUsuario");
        confirmar = findViewById(R.id.btnConfirmarC);
        cancelar = findViewById(R.id.btnCancelarC);
        textCompra = findViewById(R.id.desCompra);
        textPrecios = findViewById(R.id.textPrecios);
        miprogreso = new ProgressDialog(this);

        permisosUbicacion();
        cargarDatos(idUsuario);

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
          finish();;
            }
        });
    }

    private void permisosUbicacion(){
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        CONTEXT_INCLUDE_CODE);
                return;
            }else{
                System.out.println("Accediendo...");
            }


        }catch (Exception e){
            System.out.println("Error "+e);
        }
    }

    void cargarDatos(final String idU){
        miprogreso.setMessage("Espera...");
        miprogreso.show();
        final DatabaseReference dbRefCarrito = FirebaseDatabase.getInstance().getReference("usuario/"+idU+"/carrito/producto/"); //+idU+"/carrito/producto
        dbRefCarrito.goOnline();
        dbRefCarrito.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //NumCarrito.setText("TU CARRITO ("+dataSnapshot.getChildrenCount()+" items)");
                for (final DataSnapshot snapshot : dataSnapshot.getChildren()){

                    dbRefCarrito.child(snapshot.getKey()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            datosCarrito = snapshot.getValue(carrito.class);
                            compraLista = compraLista + "\n• "+datosCarrito.getCantidad()+" , "+datosCarrito.getTipo()+" , $ "+formatter.format(Double.parseDouble(datosCarrito.getPrecioTotal()))+" MXN";
                            total=total+Double.parseDouble(datosCarrito.getPrecioTotal());
                            textCompra.setText(compraLista);
                            textPrecios.setText("\n- SUBTOTAL : $ "+formatter.format(total-(total*0.16))+" MXN\n- IVA : $ "+formatter.format(total*0.16)+" MXN\n- ENVÍO : $ "+formatter.format(25.00)+" MXN\n- TOTAL : $ "+formatter.format(total+25.00)+" MXN");

                            confirmar.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent i = new Intent(v.getContext(),MapsActivity.class);
                                    i.putExtra("compraLista",compraLista);
                                    i.putExtra("precios",total);
                                    i.putExtra("recibo",textPrecios.getText());
                                    i.putExtra("idUsuario",idU);
                                    startActivity(i);
                                    finish();
                                }
                            });
                         miprogreso.dismiss();
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        dbRefCarrito.goOffline();
    }
}
