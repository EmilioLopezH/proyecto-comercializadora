package com.example.huevoapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.huevoapp.ui.home.adaptador;
import com.example.huevoapp.ui.home.producto;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class creaActivity extends AppCompatActivity {
EditText password,telefono;
TextView aviso;
Button iniciarSesion;
    public ProgressDialog miprogreso;

int cont=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_crea);
        password=findViewById(R.id.passwordIniciar);
        telefono=findViewById(R.id.telefonoIniciar);
        iniciarSesion=findViewById(R.id.btnIniciarSesion);
        aviso=findViewById(R.id.textAviso);
        aviso.setVisibility(View.GONE);
        miprogreso = new ProgressDialog(this);
        iniciarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Se ha pulsado");
                buscar();

            }
        });


    }
    private void buscar(){
        miprogreso.setMessage("Espera...");
        miprogreso.show();
        final DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("repartidor/");
        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
        dbRef.goOnline();
        dbRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot : dataSnapshot.getChildren()){
                    dbRef.child(snapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if(dataSnapshot.child("telefono").getValue()!=null){
                            System.out.println(dataSnapshot.child("telefono").getValue().toString());
                                System.out.println("cont "+cont);
                            if(dataSnapshot.child("telefono").getValue().toString().matches(telefono.getText().toString()) &&
                                    dataSnapshot.child("password").getValue().toString().matches(password.getText().toString())){
                                cont++;
                            }
                                if(cont==0){
                                    aviso.setVisibility(View.VISIBLE);
                                    System.out.println("no puede entrar");
                                    aviso.setText("Ingrese datos correctos");
                                    miprogreso.dismiss();

                                }else {

                                    aviso.setVisibility(View.GONE);
                                    System.out.println("puede entrar");

                                    guardarPreferenciasRep(""+dataSnapshot.getKey());
                                    miprogreso.dismiss();

                                    Intent i = new Intent(getApplicationContext(),repartidorMainActivity.class);
                                    i.putExtra("idRepartidor",""+dataSnapshot.getKey());
                                    startActivity(i);


                                    finish();

                                }cont=0;

                            }}

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        dbRef.goOffline();

        final DatabaseReference dbRef2 = FirebaseDatabase.getInstance().getReference("usuario/");
        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
        dbRef2.goOnline();
        dbRef2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot : dataSnapshot.getChildren()){
                    dbRef2.child(snapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            System.out.println(dataSnapshot.child("telefono").getValue().toString());
                            System.out.println("cont "+cont);

                            if(dataSnapshot.child("telefono").getValue().toString().matches(telefono.getText().toString()) &&
                                    dataSnapshot.child("password").getValue().toString().matches(password.getText().toString())){
                                cont++;
                            }
                            if(cont==0){
                                miprogreso.dismiss();

                                aviso.setVisibility(View.VISIBLE);
                                System.out.println("no puede entrar");
                                aviso.setText("Ingrese datos correctos");

                            }else{

                                aviso.setVisibility(View.GONE);
                                System.out.println("puede entrar");

                                guardarPreferenciasUser(""+dataSnapshot.getKey());
                                miprogreso.dismiss();

                                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                                i.putExtra("idUsuario",""+dataSnapshot.getKey());
                                startActivity(i);


                              finish();

                            }cont=0;
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        dbRef2.goOffline();

        final DatabaseReference dbRef3 = FirebaseDatabase.getInstance().getReference("administrador");
        //Toast.makeText(getActivity() , "dbRef: "+dbRef, Toast.LENGTH_LONG).show();
        dbRef3.goOnline();
        dbRef3.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (final DataSnapshot snapshot : dataSnapshot.getChildren()){

                    dbRef3.child(snapshot.getKey()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                            System.out.println(dataSnapshot.child("telefono").getValue().toString());
                            System.out.println("cont "+cont);

                            if(dataSnapshot.child("telefono").getValue().toString().matches(telefono.getText().toString()) &&
                                    dataSnapshot.child("password").getValue().toString().matches(password.getText().toString())){
                                cont++;
                            }
                            if(cont==0){
                                miprogreso.dismiss();

                                aviso.setVisibility(View.VISIBLE);
                                System.out.println("no puede entrar");
                                aviso.setText("Ingrese datos correctos");

                            }else{

                                aviso.setVisibility(View.GONE);
                                System.out.println("puede entrar");

                               // guardarPreferenciasAdmin(""+dataSnapshot.getKey());
                                miprogreso.dismiss();

                               // Intent i = new Intent(getApplicationContext(),MainActivity.class);
                              //  i.putExtra("idUsuario",""+dataSnapshot.getKey());
                              //  startActivity(i);


                                finish();
                            }cont=0;
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        dbRef3.goOffline();


    }
    private void guardarPreferenciasAdmin(String prefer){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        //idUsuario = prefer;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("admin",prefer);
        editor.commit();
    }
    private void guardarPreferenciasUser(String prefer){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        //idUsuario = prefer;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("usuario",prefer);
        editor.commit();
    }
    private void guardarPreferenciasRep(String prefer){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        //idUsuario = prefer;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("repartidor",prefer);
        editor.commit();
    }
}
