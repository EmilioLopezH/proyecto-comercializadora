package com.example.huevoapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.huevoapp.ui.notifications.usuario;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.button.MaterialButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class sesionActivity extends AppCompatActivity {
private Button btnSesion, btnContinuar, btnRegistro;
private CheckBox radioTerminos;
private EditText editTelefono, editNombre, editPassword,editPassword2;
    private String idUsuario=null, palabra="m";
    private boolean autorizar=true;
    ProgressDialog miprogreso;
    int cont=0,cont2=0;
    static int contAux=0,contAux2=0;
    ArrayList<usuario> listaUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//will hide the title
        getSupportActionBar().hide(); //hide the title bar
        setContentView(R.layout.activity_sesion);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        btnSesion=findViewById(R.id.btnSesion);
        btnContinuar=findViewById(R.id.btnContinuar);
        btnRegistro=findViewById(R.id.btnRegistro);
        editTelefono=findViewById(R.id.telefonoRegistro);
        editNombre=findViewById(R.id.nombreRegistro);
        editPassword=findViewById(R.id.passwordRegistro);
        editPassword2=findViewById(R.id.passwordRegistro2);
        radioTerminos=findViewById(R.id.checkBox);
        miprogreso = new ProgressDialog(this);


        btnSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               // guardarPreferencias("-M8QuuH5JFvjRTt-uxaw");

                Intent i = new Intent(v.getContext(),creaActivity.class);
               // i.putExtra("idUsuario",idUsuario);
                startActivity(i);

                finish();
            }
        });
        btnContinuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                guardarPreferencias("nadie");

                Intent i = new Intent(v.getContext(),MainActivity.class);
                i.putExtra("idUsuario",idUsuario);
                startActivity(i);

                finish();
            }
        });
        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                registrarUsuario();





               crearUsuario();
            }
        });

    }

    private void guardarPreferencias(String prefer){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        idUsuario = prefer;
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("usuario",prefer);
        editor.commit();
    }

    private void cargarPreferencias(){
        SharedPreferences preferences = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String usuario=preferences.getString("usuario",null);
        idUsuario=usuario;
    }

    private void registrarUsuario(){
        cont=0;
        if(editPassword.getText().toString().matches("") || editNombre.getText().toString().matches("") || editTelefono.getText().toString().matches("")){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(sesionActivity.this);
            View mV = getLayoutInflater().inflate(R.layout.dialog_direccion,null);
            final EditText editDireccion = mV.findViewById(R.id.editDireccion);
            Button aceptar = mV.findViewById(R.id.btnAceptarD);
            Button cancelar = mV.findViewById(R.id.btnCancelarD);
            cancelar.setVisibility(View.GONE);
            editDireccion.setVisibility(View.GONE);
            TextView texto = mV.findViewById(R.id.textView12);
            texto.setText("Completa todos los campos por favor");
            mBuilder.setView(mV);
            final AlertDialog dialog = mBuilder.create();
            dialog.show();
            aceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });

            cancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }else{
            cont++;
        } if(!editPassword.getText().toString().matches(editPassword2.getText().toString())){
            System.out.println(editPassword.getText().toString());
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(sesionActivity.this);
            View mV = getLayoutInflater().inflate(R.layout.dialog_direccion,null);
            final EditText editDireccion = mV.findViewById(R.id.editDireccion);
            Button aceptar = mV.findViewById(R.id.btnAceptarD);
            Button cancelar = mV.findViewById(R.id.btnCancelarD);
            cancelar.setVisibility(View.GONE);
            editDireccion.setVisibility(View.GONE);
            TextView texto = mV.findViewById(R.id.textView12);
            texto.setText("Las contraseñas no coinciden");
            mBuilder.setView(mV);
            final AlertDialog dialog = mBuilder.create();
            dialog.show();
            aceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });

            cancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }else{
            cont++;
        } if(!radioTerminos.isChecked()){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(sesionActivity.this);
            View mV = getLayoutInflater().inflate(R.layout.dialog_direccion,null);
            final EditText editDireccion = mV.findViewById(R.id.editDireccion);
            Button aceptar = mV.findViewById(R.id.btnAceptarD);
            Button cancelar = mV.findViewById(R.id.btnCancelarD);
            cancelar.setVisibility(View.GONE);
            editDireccion.setVisibility(View.GONE);
            TextView texto = mV.findViewById(R.id.textView12);
            texto.setText("Acepta los términos y condiciones");
            mBuilder.setView(mV);
            final AlertDialog dialog = mBuilder.create();
            dialog.show();
            aceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });

            cancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }else{
            cont++;
        }
        if(!Pattern.matches("[0-9]{10}",editTelefono.getText().toString())){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(sesionActivity.this);
            View mV = getLayoutInflater().inflate(R.layout.dialog_direccion,null);
            final EditText editDireccion = mV.findViewById(R.id.editDireccion);
            Button aceptar = mV.findViewById(R.id.btnAceptarD);
            Button cancelar = mV.findViewById(R.id.btnCancelarD);
            cancelar.setVisibility(View.GONE);
            editDireccion.setVisibility(View.GONE);
            TextView texto = mV.findViewById(R.id.textView12);
            texto.setText("Ingresa un número de celular válido");
            mBuilder.setView(mV);
            final AlertDialog dialog = mBuilder.create();
            dialog.show();
            aceptar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    dialog.dismiss();
                }
            });

            cancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
        }else{
            cont++;
        }

    }

    private void crearUsuario() {
        if(cont==4){
    System.out.println("Se ha creado el usuario");
    DatabaseReference dbRefUsuario = FirebaseDatabase.getInstance().getReference("usuario");
    dbRefUsuario.goOnline();
    Map<String, Object> datos = new HashMap<>();
    final String llave = dbRefUsuario.push().getKey();
    datos.put("nombre",editNombre.getText().toString());
    datos.put("direccion","ingresa tu direccion");
    datos.put("password",editPassword.getText().toString());
    datos.put("telefono",editTelefono.getText().toString());
    dbRefUsuario.child(llave).setValue(datos).addOnSuccessListener(new OnSuccessListener<Void>() {
        @Override
        public void onSuccess(Void aVoid) {
            guardarPreferencias(llave);

            Intent i = new Intent(getApplicationContext(),MainActivity.class);
            i.putExtra("idUsuario",llave);
            startActivity(i);

            finish();
        }
    });
    dbRefUsuario.goOffline();

}

    }
}
